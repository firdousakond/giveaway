package com.giveaway.pak.util.converter

import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

class ObjectConverter {

    @TypeConverter
    fun fromIntList(intList: List<Int>?): String? {
        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter<List<Int>>(List::class.java)

        return jsonAdapter.toJson(intList)
    }

    @TypeConverter
    fun toIntList(intListStr: String?): List<Int>? {
        val moshi = Moshi.Builder().build()
        val type = Types.newParameterizedType(List::class.java, Integer::class.java)
        val adapter = moshi.adapter<List<Int>>(type)
        return adapter.fromJson(intListStr)
    }

}