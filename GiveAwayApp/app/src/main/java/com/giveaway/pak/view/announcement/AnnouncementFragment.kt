package com.giveaway.pak.view.announcement

import android.os.Bundle
import android.view.View
import com.giveaway.pak.BR
import com.giveaway.pak.R
import com.giveaway.pak.databinding.FragmentAnnouncementBinding
import com.giveaway.pak.view.base.BaseBindingFragment
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_announcement.*
import javax.inject.Inject

class AnnouncementFragment :
    BaseBindingFragment<FragmentAnnouncementBinding, AnnouncementViewModel, BaseHandler>() {

    @Inject
    lateinit var announcementViewModel: AnnouncementViewModel

    override fun getBindingVariable(): Int = BR.viewmodel

    override fun getLayoutId(): Int = R.layout.fragment_announcement

    override fun getViewModel(): AnnouncementViewModel? = announcementViewModel



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBindings()
        initComponents()
        initObserver()
        setListener()
    }

    private fun initObserver() {
    }

    private fun setListener() {
        tvWinners.setOnClickListener {
            tvWinners.isSelected = true
            tvReviews.isSelected = false
        }
        tvReviews.setOnClickListener {
            tvReviews.isSelected = true
            tvWinners.isSelected = false
        }
    }

    override fun initComponents() {
        super.initComponents()
        if(activity is HomeActivity){
            (activity as HomeActivity).setToolbarTitle(getString(R.string.announcement))
            (activity as HomeActivity).hideLogoutIcon()
        }
        tvWinners.isSelected = true
    }


}