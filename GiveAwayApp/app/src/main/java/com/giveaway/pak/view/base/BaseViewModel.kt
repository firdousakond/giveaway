
package com.giveaway.pak.view.base

import androidx.lifecycle.ViewModel
import java.lang.ref.WeakReference

abstract class BaseViewModel<N> : ViewModel() {

    private var mHandler: WeakReference<N>? = null

    var handler: N?
        get() = mHandler?.get()
        set(navigator) {
            this.mHandler = WeakReference(navigator!!)
        }
}
