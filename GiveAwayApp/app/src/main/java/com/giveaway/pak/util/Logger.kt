package com.giveaway.pak.util

import android.os.Environment
import android.util.Log

import com.giveaway.pak.BuildConfig

import java.io.BufferedWriter
import java.io.FileWriter
import java.io.PrintWriter
import java.util.Date

class Logger private constructor() {

    init {
        throw IllegalStateException("Logger class")
    }

    companion object {

        private const val APP_ID = "snaaap_merchant"
        private const val LOG_DIR = "/" + BuildConfig.APPLICATION_ID
        private const val LOG_FILE_NAME = "/" + "snaaap_merchant.txt"
        private const val WRITE_LOGS_TO_FILE = false
        private const val LOG_LEVEL_VERBOSE = 4
        private const val LOG_LEVEL_DEBUG = 3
        private const val LOG_LEVEL_INFO = 2
        private const val LOG_LEVEL_ERROR = 1
        private const val LOG_LEVEL_OFF = 0
        private const val CURRENT_LOG_LEVEL = LOG_LEVEL_VERBOSE

        private fun log(message: String, logLevel: Int) {
            if (logLevel > CURRENT_LOG_LEVEL) {
                return
            }
            Log.v(APP_ID, message)
        }

        private fun writeToFile(message: String) {
            val sdCard = Environment.getExternalStorageDirectory()
            val dir = FileUtils.getFile(sdCard.absolutePath + LOG_DIR)
            dir.mkdirs()
            val file = FileUtils.getFile(dir, LOG_FILE_NAME)
            try {
                PrintWriter(
                    BufferedWriter(
                        FileWriter(file, true), 8 * 1024
                    )
                ).use { writer ->
                    writer.println(
                        APP_ID + " " + Date().toString() + " : "
                                + message
                    )
                }
            } catch (e: Exception) {
                e.message?.let { debug(it) }
            }

        }

        fun logOff(message: String) {
            log(message, LOG_LEVEL_OFF)
        }

        fun verbose(message: String) {
            log(message, LOG_LEVEL_VERBOSE)
        }

        fun debug(message: String) {
            log(message, LOG_LEVEL_DEBUG)
        }

        fun error(message: String) {
            log(message, LOG_LEVEL_ERROR)
        }

        fun info(message: String) {
            log(message, LOG_LEVEL_INFO)
        }

        fun write(message: String) {
            logWriteToFile(message)
        }

        private fun logWriteToFile(message: String) {

            writeToFile(message)


        }
    }


}
