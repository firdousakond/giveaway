package com.giveaway.pak.view.home

import android.content.Intent
import android.os.Bundle
import com.giveaway.pak.BR
import com.giveaway.pak.R
import com.giveaway.pak.databinding.ActivityHomeBinding
import com.giveaway.pak.util.*
import com.giveaway.pak.view.NavigationManager
import com.giveaway.pak.view.announcement.AnnouncementFragment
import com.giveaway.pak.view.base.BaseBindingActivity
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.giveaway.AddGiveawayActivity
import com.giveaway.pak.view.giveaway.GiveawayFragment
import com.giveaway.pak.view.settings.SettingsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_toolbar.*
import javax.inject.Inject

class HomeActivity : BaseBindingActivity<ActivityHomeBinding,HomeViewModel,BaseHandler>() {


    @Inject
    lateinit var homeViewModel : HomeViewModel

    override fun getViewModel(): HomeViewModel?  = homeViewModel
    override fun getBindingVariable(): Int  = BR.viewmodel
    override fun getLayoutId(): Int  = R.layout.activity_home



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tvHeader.text = getString(R.string.giveaways)
        setListeners()
        replace(GiveawayFragment().javaClass, null)
    }

    private fun setListeners(){
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener)
        ivAdd.setOnClickListener {
            NavigationManager.openClass(this, AddGiveawayActivity::class.java, null)
        }
    }

    private val navigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        when(it.itemId){
            R.id.navigation_giveaway->{
                if(peek() !is GiveawayFragment) {
                    replace(GiveawayFragment().javaClass, null)
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_announcement->{
                if(peek() !is AnnouncementFragment) {
                    replace(AnnouncementFragment().javaClass, null)
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_more->{
                if(peek() !is SettingsFragment){
                    replace(SettingsFragment().javaClass,null)
                }
                return@OnNavigationItemSelectedListener true
            }
            else-> {
            return@OnNavigationItemSelectedListener false
        }

    }
        
    }

    fun setToolbarTitle(title: String){
        tvHeader.text = title
    }
    fun hideAddIcon(){
        ivAdd.hide()
        ivLogout.show()
    }
    fun hideLogoutIcon(){
        ivAdd.show()
        ivLogout.hide()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if(intent != null){
            val isMyEntries = intent.getBooleanExtra(IS_MY_ENTRIES,false)
            val isMyGiveaway = intent.getBooleanExtra(IS_MY_GIVEAWAY,false)
            val isWonGiveaway = intent.getBooleanExtra(IS_WON_GIVEAWAY,false)
            val bundle = Bundle()
            bundle.putBoolean(IS_MY_ENTRIES, isMyEntries)
            bundle.putBoolean(IS_MY_GIVEAWAY, isMyGiveaway)
            bundle.putBoolean(IS_WON_GIVEAWAY, isWonGiveaway)
            val giveawayFragment = GiveawayFragment.newInstance()
            giveawayFragment.arguments = bundle
            replace(giveawayFragment::class.java,bundle)
            bottomNavigation.menu.findItem(R.id.navigation_giveaway).isChecked = true
        }
    }

}