package com.giveaway.pak.model


data class CityModel(
    val cities: List<String>? = null
)