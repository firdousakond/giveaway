package com.giveaway.pak.dagger.component

import android.app.Application
import android.content.Context
import com.giveaway.pak.GiveawayApplication
import com.giveaway.pak.dagger.module.ActivityModule
import com.giveaway.pak.dagger.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton



@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityModule::class]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun appContext(context: Context) : Builder

        fun build(): AppComponent
    }

    fun inject(giveawayApplication: GiveawayApplication)

}