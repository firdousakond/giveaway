package com.giveaway.pak.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.facebook.login.LoginManager
import com.giveaway.pak.R
import com.giveaway.pak.util.SharedPrefUtil
import com.giveaway.pak.view.home.HomeActivity
import com.giveaway.pak.view.login.LoginActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            if (SharedPrefUtil(this).isLoggedIn()) {
                NavigationManager.openClass(this, HomeActivity::class.java, null)
            } else {
                NavigationManager.openClass(this, LoginActivity::class.java, null)
            }
            finish()
        }, 2000)
    }
}