package com.giveaway.pak.util

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

class SharedPrefUtil(var context: Context) {

    private val sharedPref: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    private fun hasFacebookId(): Boolean {
        return (!sharedPref.getString(ACCESS_TOKEN, "").isNullOrBlank())
    }

    fun setHasUser() {
        with(sharedPref.edit()) {
            putBoolean(HAS_USER, true)
            apply()
        }
    }

    private fun getHasUser(): Boolean {
        return (!sharedPref.getString(USER_ID, "").isNullOrBlank())
    }

    fun deleteHasUser() {
        sharedPref.edit().remove(HAS_USER).apply()
    }

    fun deleteToken() {
        sharedPref.edit().remove(ACCESS_TOKEN).apply()
    }

    fun isLoggedIn(): Boolean {
        return hasFacebookId() && getHasUser()
    }

    fun isFirstLaunch(): Boolean {
        return (!sharedPref.getBoolean(FIRST_LAUNCH, false))
    }

    fun setFirstLaunch() {
        with(sharedPref.edit()) {
            putBoolean(FIRST_LAUNCH, true)
            apply()
        }
    }

    fun setUserId(userId: String) {
        with(sharedPref.edit()) {
            putString(USER_ID, userId)
            apply()
        }
    }

    fun getUserId(): String? {
        return sharedPref.getString(USER_ID, "")
    }

    fun setFacebookId(accessToken: String) {
        with(sharedPref.edit()) {
            putString(ACCESS_TOKEN, accessToken)
            apply()
        }
    }

    fun getFacebookId(): String? {
        return sharedPref.getString(ACCESS_TOKEN, "")
    }

}