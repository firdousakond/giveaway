package com.giveaway.pak.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import com.giveaway.pak.R
import com.giveaway.pak.util.SharedPrefUtil

class NavigationManager {

    companion object {


        fun openClass(context: Context?, aClass: Class<*>, bundle: Bundle? = null) {
            if (context == null) return
            val intent = Intent(context, aClass).apply {
                if (bundle != null) {
                    putExtras(bundle)
                }
            }
            ContextCompat.startActivity(context, intent, null)
            (context as AppCompatActivity).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }

        fun clearStack(supportFragmentManager: FragmentManager?) {
            if (supportFragmentManager == null) {
                return
            }
            //Clearing back stack mFragment entries
            val backStackEntry = supportFragmentManager.backStackEntryCount
            if (backStackEntry > 0) {
                for (i in 0 until backStackEntry) {
                    supportFragmentManager.popBackStack()
                }
            }

            //Removing all the mFragment that are shown here
            if (supportFragmentManager.fragments.size > 0) {
                for (i in 0 until supportFragmentManager.fragments.size) {
                    //  Timber.d("fragments ${supportFragmentManager.getBackStackEntryAt(i).name}")
                    val mFragment = supportFragmentManager.fragments[i]
                    if (mFragment != null) {
                        supportFragmentManager.beginTransaction().remove(mFragment).commit()
                    }
                }
            }
        }


        private fun openClass(context: Context, intent: Intent) {
            ContextCompat.startActivity(context, intent, null)
            (context as AppCompatActivity).overridePendingTransition(
                R.anim.slide_in_right,
                R.anim.slide_out_left
            )
        }

        fun logout(context: Context?) {
            if (context == null) {
                return
            }
            SharedPrefUtil(context).deleteToken()
            SharedPrefUtil(context).deleteHasUser()
        }
    }


}