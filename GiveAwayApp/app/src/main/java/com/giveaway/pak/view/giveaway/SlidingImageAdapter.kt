package com.giveaway.pak.view.giveaway

import android.content.Context
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.ViewPager
import com.giveaway.pak.R
import com.giveaway.pak.util.loadImageFromUrl

class SlidingImageAdapter(private val context: Context, private val images: List<String>?, private val imageClickListener: ImageClickListener) :
    PagerAdapter() {

    override fun getCount(): Int {

        return images?.size!!
    }
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.sliding_image_layout, null)
        val imageView = view.findViewById(R.id.image) as ImageView
        if(!images.isNullOrEmpty()) {
            imageView.loadImageFromUrl(context,images[position])
        }
        val viewPager = container as ViewPager
        viewPager.addView(view, 0)
        view.setOnClickListener { imageClickListener.onImageClick() }
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val viewPager = container as ViewPager
        val view = `object` as View
        viewPager.removeView(view)
    }


    interface ImageClickListener{
        fun onImageClick()
    }
}