package com.giveaway.pak.view.giveaway

import android.app.Activity
import android.net.Uri
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.giveaway.pak.R
import com.giveaway.pak.util.FileUtils
import com.giveaway.pak.util.Logger
import com.giveaway.pak.util.loadImageFromFile

import java.util.ArrayList

class ImageAdapter(private val activity: Activity, private val uploadDocumentsDelegate: UploadDocumentsDelegate?
) : RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private val totalDocList: MutableList<Uri>

    init {
        this.totalDocList = ArrayList()
    }

    fun addItem(imageList: List<Uri>) {
        totalDocList.addAll(imageList)
        uploadDocumentsDelegate!!.onImageAdded()
        notifyDataSetChanged()
    }

    fun deleteAll(){
        totalDocList.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {

        val v = inflater.inflate(R.layout.item_image, parent, false)
        return ImageViewHolder(v)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {

        val model: Uri
        try {
            model = getItem(position)
        } catch (e: Exception) {
            Logger.error(e.toString())
            return
        }

        setImage(model, holder.imgPhoto)
        setDeleteButton(holder.imgClear, position)

    }

    private fun setDeleteButton(imgDelete: ImageView, position: Int) {
        imgDelete.setOnClickListener(DeleteOnClickListener(position))
    }

    private fun getItem(position: Int): Uri {

        return totalDocList[position]
    }

    override fun getItemCount(): Int {
        return totalDocList.size
    }

    fun size(): Int {
        return totalDocList.size
    }

    override fun getItemId(position: Int): Long {

        Logger.debug("get item id according to position")

        return super.getItemId(position)
    }

    private fun setImage(imagePath: Uri, imageView: ImageView) {

        imageView.loadImageFromFile(activity,FileUtils.getFile(FileUtils.getRealPath(activity, imagePath)!!))
        imageView.setOnClickListener(ImageDocumentOnClickListener(imagePath.toString()))

    }

    interface UploadDocumentsDelegate {
        fun onImageRemoved(position: Int)

        fun onImageAdded()
    }

    inner class ImageViewHolder internal constructor(view: View) :
        RecyclerView.ViewHolder(view) {

        var imgPhoto: ImageView = view.findViewById(R.id.img_photo)
        var imgClear: ImageView = view.findViewById(R.id.img_clear)

    }

    private inner class DeleteOnClickListener internal constructor(private val positionToDelete: Int) :
        View.OnClickListener {

        override fun onClick(view: View) {

            deleteImage(positionToDelete)
        }

        private fun deleteImage(positionToDelete: Int) {

            totalDocList.removeAt(positionToDelete)

            uploadDocumentsDelegate?.onImageRemoved(positionToDelete)

            notifyDataSetChanged()

        }

    }

    private inner class ImageDocumentOnClickListener internal constructor(private val imagePath: String) :
        View.OnClickListener {

        override fun onClick(v: View) {

//            val intent = Intent(activity, FullScreenImageActivity::class.java)
//            intent.putExtra(AppConstant.IMAGE_PATH, imagePath)
//            intent.putExtra(AppConstant.EXTRA_IS_LOCAL_IMAGE_PATH, true)
//            intent.putExtra(AppConstant.IS_PROFILE_IMAGE, false)
//            intent.putExtra(AppConstant.HEADER, activity.getString(R.string.image))
          //  activity.startActivity(intent)

        }
    }
}