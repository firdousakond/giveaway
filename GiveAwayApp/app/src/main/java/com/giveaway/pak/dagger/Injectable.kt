package com.giveaway.pak.dagger

/**
 * Marks an activity / mFragment injectable.
 */
interface Injectable