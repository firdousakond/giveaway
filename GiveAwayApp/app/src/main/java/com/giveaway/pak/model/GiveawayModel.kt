package com.giveaway.pak.model

import com.google.firebase.firestore.PropertyName
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GiveawayModel : Serializable{

    var id: String? = null
    var userId: String? = null
    var city: String? = null
    var contactDetails: ContactDetails ? =null
    var createdDate: Long? = null
    var title: String? = null
    var description: String? = null
    var numberOfDays: String? = null
    var numberOfWinners: Int? = null
    var facebookLink: String? = null
    var instagramLink: String? = null
    var youtubeLink: String? = null
    var twitterLink: String? = null
    var tiktokLink: String? = null

    @JvmField
    @PropertyName(value = "isFacebookSelected")
    var isFacebookSelected: Boolean? = false
    @JvmField
    @PropertyName(value = "isTwitterSelected")
    var isTwitterSelected: Boolean? = false

    @JvmField
    @PropertyName(value = "isYoutubeSelected")
    var isYoutubeSelected: Boolean? = false

    @JvmField
    @PropertyName(value = "isInstagramSelected")
    var isInstagramSelected: Boolean? = false

    @JvmField
    @PropertyName(value = "isTiktokSelected")
    var isTiktokSelected: Boolean? = false

    var imageURLs: List<String>? = null
    var entries: List<Entries>? = null

}

class ContactDetails : Serializable{
    var email: String? = null
    var mobileNumber: String? = null
    var officeNumber: String? = null
}
class Entries : Serializable{
    var uid: String? = null
    var name: String? = null
    var email: String? = null
    var providerId: String? = null
    var imageURl: String? = null
}