package com.giveaway.pak.view.giveaway

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.lifecycle.Observer
import com.giveaway.pak.BR
import com.giveaway.pak.R
import com.giveaway.pak.databinding.ActivityGiveawayDetailsBinding
import com.giveaway.pak.model.GiveawayModel
import com.giveaway.pak.model.UserModel
import com.giveaway.pak.util.*
import com.giveaway.pak.view.NavigationManager
import com.giveaway.pak.view.base.BaseBindingActivity
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.common.EnterWinDialog
import com.giveaway.pak.view.common.FullImageActivity
import com.giveaway.pak.view.dialog.AppAlertDialog
import com.giveaway.pak.view.entry.EntryViewModel
import com.giveaway.pak.view.login.LoginViewModel
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_giveaway_details.*
import kotlinx.android.synthetic.main.activity_toolbar.*
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class GiveawayDetailsActivity :
    BaseBindingActivity<ActivityGiveawayDetailsBinding, GiveawayViewModel, BaseHandler>(),
    SlidingImageAdapter.ImageClickListener, EnterWinDialog.SubmitEntryListener {

    @Inject
    lateinit var giveawayViewModel: GiveawayViewModel

    @Inject
    lateinit var entryViewModel: EntryViewModel

    @Inject
    lateinit var loginViewModel: LoginViewModel

    override fun getBindingVariable(): Int = BR.viewmodel

    override fun getLayoutId(): Int = R.layout.activity_giveaway_details

    override fun getViewModel(): GiveawayViewModel? = giveawayViewModel
    private lateinit var giveawayModel: GiveawayModel
    private var countDownTimer: CountDownTimer? = null
    private var dialog : EnterWinDialog ?= null
    private var userModel: UserModel?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ivBack.show()
        getBundleData()
        setListeners()
        fetchUser()
        initObserver()
    }

    private fun fetchUser() {
        val facebookId = SharedPrefUtil(this).getFacebookId()
        loginViewModel.fetchUser(facebookId!!)
    }

    private fun initObserver() {
        entryViewModel.entryLiveData.observe(this, entryObserver)
        loginViewModel.userDetailsLiveData.observe(this, userDetailsObserver)
    }

    private val userDetailsObserver = Observer<LiveDataResult<QuerySnapshot>>{ result->
        when(result.status){
            LiveDataResult.Status.LOADING -> {
                Logger.debug("Loading")
            }
            LiveDataResult.Status.SUCCESS -> {
                onDismissLoading()
                if (result.data != null && !result.data.documents.isNullOrEmpty()) {
                    val userModel = result.data.documents[0].toObject(UserModel::class.java)
                    if (userModel != null) {
                        this.userModel = userModel
                    }
                }
            }

            LiveDataResult.Status.ERROR -> {
                Logger.error("Error")
            }

        }

    }

    private fun setListeners() {
        btnEnterWin.setOnClickListener { showEntriesDialog() }
    }

    private var entryObserver = Observer<LiveDataResult<Boolean>>{ result ->
        when (result.status) {
            LiveDataResult.Status.LOADING -> {
                Logger.debug("Loading")
            }
            LiveDataResult.Status.SUCCESS -> {
                onDismissLoading()
                object : AppAlertDialog(this@GiveawayDetailsActivity) {
                    override fun onPositiveButton() {
                        super.onPositiveButton()
                        finish()
                    }
                }.apply {
                    setTitle("Success")
                    setMessage(getString(R.string.success_msg_add_entry))
                    show()
                }
            }
            LiveDataResult.Status.ERROR -> {
                onDismissLoading()
                CommonUtil.displaySnackbar(
                    this,
                    getString(R.string.common_error_msg),
                    MessageType.ERROR
                )
            }

        }
    }

    private fun showEntriesDialog() {
        dialog = EnterWinDialog(this).newInstance(giveawayModel)
        dialog?.show(supportFragmentManager, "Dialog")
    }

    private fun getBundleData() {
        val bundle = intent.extras
        if (bundle != null) {
            giveawayModel = bundle.getSerializable(GIVEAWAY_MODEL) as GiveawayModel
            setData()

        }
    }

    private fun setData() {
        setImageSlider()
        tvHeader.text = giveawayModel.title
        tvTitle.text = giveawayModel.title
        tvLocation.text = giveawayModel.city
        tvDescription.text = giveawayModel.description
        var numWinners: String? = giveawayModel.numberOfWinners.toString()
        if (numWinners.isNullOrEmpty()) {
            numWinners = "0"
        }

        tvNumWinners.text = numWinners
        tvTotalWinner.text = numWinners
        if (giveawayModel.imageURLs?.size!! < 2) {
            indicator.hide()
        }
        var numEntries = 0
        if (!giveawayModel.entries.isNullOrEmpty()) {
            numEntries = giveawayModel.entries?.size!!
        }
        val entries = "${getString(R.string.entries)} $numEntries"
        tvEntries.text = entries

        val createdDateMillis = giveawayModel.createdDate

        val utcTime =
            createdDateMillis?.plus(1000 * 60 * 60 * 24 * giveawayModel.numberOfDays?.toInt()!!)

        val cal = Calendar.getInstance()
        val offset = cal.timeZone.getOffset(utcTime!!)
        val lastDate =  utcTime - offset
        val remainingMillis = lastDate.minus(System.currentTimeMillis())
        if (remainingMillis > 0) {
            btnEnterWin.show()
            startGiveawayTimer(remainingMillis)
        } else {
            btnEnterWin.hide()
            tvEndedMessage.show()
            tvTimer.text = getString(R.string.giveaway_ended)
        }
        val userId = SharedPrefUtil(this).getUserId()
        giveawayModel.entries?.forEach {
            if(it.uid == userId){
                tvEndedMessage.show()
                btnEnterWin.hide()
                return@forEach
            }
        }
    }


    private fun startGiveawayTimer(countdownMillis: Long) {
        countDownTimer = object : CountDownTimer(countdownMillis, 1000) {
            override fun onFinish() {
                tvTimer.hide()
            }

            override fun onTick(millisUntilFinished: Long) {
                tvTimer.text = String.format(
                    "%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) -
                            TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(
                                    millisUntilFinished
                                )
                            ),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                            TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(
                                    millisUntilFinished
                                )
                            )
                )
            }

        }.start()
    }


    private fun setImageSlider() {

        val imageAdapter =
            SlidingImageAdapter(this, giveawayModel.imageURLs, this)
        vpImages.adapter = imageAdapter
        indicator.setupWithViewPager(vpImages)
        val timer = Timer()
        timer.scheduleAtFixedRate(SliderTimer(), 3000, 3000)

    }

    private inner class SliderTimer : TimerTask() {

        override fun run() {
            this@GiveawayDetailsActivity.runOnUiThread {
                if (vpImages.currentItem < giveawayModel.imageURLs?.size!!.minus(1)) {
                    vpImages.currentItem = vpImages.currentItem + 1
                } else {
                    vpImages.currentItem = 0
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (countDownTimer != null) {
            countDownTimer?.cancel()
        }
    }

    override fun onImageClick() {
        val bundle = Bundle()
        bundle.putSerializable(GIVEAWAY_MODEL, giveawayModel)
        NavigationManager.openClass(this, FullImageActivity::class.java, bundle)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        dialog?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSubmitEntry() {
        entryViewModel.addEntries(giveawayModel, userModel)
    }
}