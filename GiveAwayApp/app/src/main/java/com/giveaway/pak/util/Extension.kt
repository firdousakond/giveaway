package com.giveaway.pak.util

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.PagerTabStrip
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.Headers
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.giveaway.pak.R
import java.io.File


fun ImageView.loadImageFromUrl(
    context: Context?,
    imageUrl: String) {
        val requestHeaders: Headers = LazyHeaders.Builder()
            .build()
        val glideUrl = GlideUrl(imageUrl, requestHeaders)
        Glide.with(context!!)
            .load(glideUrl).apply(
                RequestOptions().timeout(CONSTANT_GLIDE_TIMEOUT)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
            )
            .dontTransform()
            .listener(object : RequestListener<Drawable?> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: Target<Drawable?>,
                    isFirstResource: Boolean
                ): Boolean {
                    this@loadImageFromUrl.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.giveaway_logo))
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any,
                    target: Target<Drawable?>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {

                    return false
                }
            })
            .placeholder(R.drawable.progress_animation).error(R.drawable.giveaway_logo)
            .into(this)
}



fun ImageView.loadImageFromFile(
    context: Context?,
    filePath: File) {

    Glide.with(context!!)
        .load(filePath).apply(
            RequestOptions().timeout(CONSTANT_GLIDE_TIMEOUT)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
        )
        .listener(object : RequestListener<Drawable?> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any,
                target: Target<Drawable?>,
                isFirstResource: Boolean
            ): Boolean {
                this@loadImageFromFile.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.default_image))
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any,
                target: Target<Drawable?>,
                dataSource: DataSource,
                isFirstResource: Boolean
            ): Boolean {
                this@loadImageFromFile.setImageDrawable(resource)
                return false
            }
        })
        .placeholder(R.drawable.progress_animation).error(R.drawable.default_image)
        .into(this)
}



fun View.show (){
    visibility = View.VISIBLE
}

fun View.hide (){
    visibility = View.GONE
}

fun View.invisible (){
    visibility = View.INVISIBLE
}

fun View.isVisible() : Boolean {
    return visibility == View.VISIBLE
}

fun View.enable() : Boolean {
    return isEnabled
}

fun View.disable() : Boolean {
    return !isEnabled
}


fun RecyclerView.initialize(adapter: RecyclerView.Adapter<*>?) {
    setAdapter(adapter)
    layoutManager = LinearLayoutManager(context)
}

fun RecyclerView.initializeHorizontal(adapter: RecyclerView.Adapter<*>?) {
    setAdapter(adapter)
    layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
}


fun RecyclerView.initializeGridHorizontal(adapter: RecyclerView.Adapter<*>?, gridCount: Int = 1) {
    setAdapter(adapter)
    layoutManager = GridLayoutManager(context, gridCount, GridLayoutManager.HORIZONTAL, false)
}

fun RecyclerView.initializeGridVertical(adapter: RecyclerView.Adapter<*>?, gridCount: Int = 1) {
    setAdapter(adapter)
    layoutManager = GridLayoutManager(context, gridCount, GridLayoutManager.VERTICAL, false)
}


fun PagerTabStrip.init() {
    setTabIndicatorColorResource(R.color.colorPrimary)
    setTextColor(ContextCompat.getColor(context, android.R.color.black))
    drawFullUnderline = true

}

fun View.finishActivity(activity : Activity){
    activity.finish()
}

fun SwipeRefreshLayout.setLoadColors(){
        setColorSchemeColors(
        ContextCompat.getColor(context, R.color.colorPrimary),
        ContextCompat.getColor(context, R.color.colorAccent)
    )

}

fun Context.toastMessage(message: String? ) {

    val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
    val textView = toast.view.findViewById(android.R.id.message) as TextView
    textView.maxLines = 2
    textView.textSize = 13f
    textView.setTextColor(Color.WHITE)
    toast.show()

}


