package com.giveaway.pak.view.giveaway

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.giveaway.pak.model.GiveawayModel
import com.giveaway.pak.util.LiveDataResult
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.base.BaseViewModel
import com.google.firebase.firestore.*
import com.google.firebase.storage.StorageMetadata
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class GiveawayViewModel @Inject constructor(
    private var firestore: FirebaseFirestore,
    private var storageReference: StorageReference
) :
    BaseViewModel<BaseHandler>() {


    var addGiveawayLiveData = MutableLiveData<LiveDataResult<Boolean>>()
    var imageUrlLiveData = MutableLiveData<LiveDataResult<Uri>>()
    var giveawayLiveData = MutableLiveData<LiveDataResult<QuerySnapshot>>()
    var snapshotListener : ListenerRegistration? = null

    fun fetchGiveaway() {
        val collectionReference = firestore.collection("giveaway")
        GlobalScope.launch(Dispatchers.IO) {

            val query = collectionReference.orderBy("createdDate", Query.Direction.DESCENDING)

            snapshotListener = query.addSnapshotListener { snapshot, error ->

                if (error != null) {
                    giveawayLiveData.postValue(LiveDataResult.error(error))
                    return@addSnapshotListener
                }

                if (snapshot != null && !snapshot.isEmpty) {
                    giveawayLiveData.postValue(LiveDataResult.success(snapshot))

                }

            }

        }
    }


    fun uploadFile(filePath: Uri) {
        imageUrlLiveData.postValue(LiveDataResult.loading())
        viewModelScope.launch {
            val storageMetadata = StorageMetadata.Builder()
            storageMetadata.contentType = "application/octet-stream"
            val reference = storageReference.child(
                "giveaway/" + UUID.randomUUID().toString().toUpperCase(
                    Locale.ENGLISH
                ) + ".jpg"
            )

            reference.putFile(filePath, storageMetadata.build())
                .addOnSuccessListener {
                    reference.downloadUrl.addOnSuccessListener {
                        imageUrlLiveData.postValue(LiveDataResult.success(it))
                    }
                }
                .addOnFailureListener {
                    imageUrlLiveData.postValue(LiveDataResult.error(it))
                }
        }
    }


    fun addGiveaway(giveawayModel: GiveawayModel) {
        viewModelScope.launch(Dispatchers.IO) {
            addGiveawayLiveData.postValue(LiveDataResult.loading())
            firestore.collection("giveaway").document(giveawayModel.id!!)
                .set(giveawayModel)
                .addOnSuccessListener {
                    addGiveawayLiveData.postValue(LiveDataResult.success(true))
                }
                .addOnFailureListener {
                    addGiveawayLiveData.postValue(LiveDataResult.error(it))
                }
        }
    }

    fun fetchMyGiveaway(userId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            giveawayLiveData.postValue(LiveDataResult.loading())
            val docReference = firestore.collection("giveaway").whereEqualTo("userId", userId)
            docReference.get()
                .addOnSuccessListener {
                    giveawayLiveData.postValue(LiveDataResult.success(it))
                }.addOnFailureListener {
                    giveawayLiveData.postValue(LiveDataResult.error(it))
                }
        }
    }

}