package com.giveaway.pak.view.settings

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.giveaway.pak.BR
import com.giveaway.pak.R
import com.giveaway.pak.databinding.FragmentSettingsBinding
import com.giveaway.pak.model.UserModel
import com.giveaway.pak.util.*
import com.giveaway.pak.view.base.BaseBindingFragment
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.home.HomeActivity
import com.giveaway.pak.view.login.LoginViewModel
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.fragment_settings.*
import javax.inject.Inject

class SettingsFragment :
    BaseBindingFragment<FragmentSettingsBinding, SettingsViewModel, BaseHandler>() {

    @Inject
    lateinit var settingsViewModel: SettingsViewModel

    @Inject
    lateinit var loginViewModel: LoginViewModel

    override fun getBindingVariable(): Int = BR.viewmodel

    override fun getLayoutId(): Int = R.layout.fragment_settings

    override fun getViewModel(): SettingsViewModel? = settingsViewModel
    private var facebookId = ""
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBindings()
        initComponents()
        initObserver()
        setListener()
        facebookId = SharedPrefUtil(requireContext()).getFacebookId()!!
        loginViewModel.fetchUser(facebookId)
    }

    private fun initObserver() {
        loginViewModel.userDetailsLiveData.observe(viewLifecycleOwner, userDetailsObserver)
    }

    private val userDetailsObserver = Observer<LiveDataResult<QuerySnapshot>>{ result->
        when(result.status){
            LiveDataResult.Status.LOADING -> {
                Logger.debug("Loading")
            }
            LiveDataResult.Status.SUCCESS -> {
                onDismissLoading()
                if (result.data != null && !result.data.documents.isNullOrEmpty()) {
                    val userModel = result.data.documents[0].toObject(UserModel::class.java)
                    if (userModel != null) {
                        setUserDetails(userModel)
                    }
                }
            }

            LiveDataResult.Status.ERROR -> {
                Logger.error("Error")
            }

        }

    }

    private fun setUserDetails(userModel: UserModel) {
        if(!userModel.imageURL.isNullOrEmpty()) {
            ivProfile.loadImageFromUrl(requireContext(), userModel.imageURL!!)
        }
        tvName.text = userModel.name
    }

    private fun setListener() {
        tvMyEntries.setOnClickListener { showMyEntries() }
        tvMyGiveaways.setOnClickListener { showMyGiveaway() }
    }

    private fun showMyGiveaway() {
        val intent = Intent(activity,HomeActivity::class.java)
        intent.putExtra(IS_MY_GIVEAWAY,true)
        startActivity(intent)
    }

    private fun showMyEntries() {
        val intent = Intent(activity,HomeActivity::class.java)
        intent.putExtra(IS_MY_ENTRIES,true)
        startActivity(intent)
    }

    override fun initComponents() {
        super.initComponents()
        if(activity is HomeActivity){
            (activity as HomeActivity).setToolbarTitle(getString(R.string.settings))
            (activity as HomeActivity).hideAddIcon()
        }
    }


}