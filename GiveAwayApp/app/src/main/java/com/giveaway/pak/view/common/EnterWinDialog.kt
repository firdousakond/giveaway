package com.giveaway.pak.view.common

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.giveaway.pak.R
import com.giveaway.pak.model.GiveawayModel
import com.giveaway.pak.util.*
import com.giveaway.pak.view.dialog.AppAlertDialog
import kotlinx.android.synthetic.main.dialog_enter_win.*


class EnterWinDialog(private val submitEntryListener: SubmitEntryListener) : DialogFragment(), View.OnClickListener {

    companion object{
        private const val FACEBOOK_REQUEST_CODE = 100
        private const val INSTAGRAM_REQUEST_CODE = 200
        private const val YOUTUBE_REQUEST_CODE = 300
        private const val TWITTER_REQUEST_CODE = 400
        private const val TIKTOK_REQUEST_CODE = 500

    }

    fun newInstance(giveawayModel: GiveawayModel?): EnterWinDialog {
        val args = Bundle()
        args.putSerializable(GIVEAWAY_MODEL, giveawayModel)
        val fragment = EnterWinDialog(submitEntryListener)
        fragment.arguments = args
        return fragment
    }
    private var linkCount = 0
    private var giveawayModel: GiveawayModel? = null

     override fun onCreateView(
         inflater: LayoutInflater,
         container: ViewGroup?,
         savedInstanceState: Bundle?
     ): View? {
         return inflater.inflate(R.layout.dialog_enter_win,container,false)
     }
     override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
         super.onViewCreated(view, savedInstanceState)
         giveawayModel = arguments?.getSerializable(GIVEAWAY_MODEL) as GiveawayModel
         setListeners()
         if (giveawayModel != null) {
             setEntries()
         }
     }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog?.window?.setBackgroundDrawableResource(R.drawable.shape_dialog_bg)
        dialog?.window?.setLayout(1000,WindowManager.LayoutParams.WRAP_CONTENT)
    }

    private fun setListeners() {
        ivClose?.setOnClickListener { dismiss() }
        btnSubmitEntries?.setOnClickListener(this)
        clFacebook?.setOnClickListener(this)
        clFacebook?.setOnClickListener(this)
        clTwitter?.setOnClickListener(this)
        clInstagram?.setOnClickListener(this)
        clYoutube?.setOnClickListener(this)
        clTikTok?.setOnClickListener(this)
    }

    private fun setEntries() {
        if (giveawayModel?.isFacebookSelected!! && !giveawayModel?.facebookLink.isNullOrEmpty()) {
            cvFacebook?.show()
            linkCount++
        }
        if (giveawayModel?.isInstagramSelected!! && !giveawayModel?.instagramLink.isNullOrEmpty()) {
            cvInstagram?.show()
            linkCount++
        }
        if (giveawayModel?.isYoutubeSelected!! && !giveawayModel?.youtubeLink.isNullOrEmpty()) {
            cvYoutube?.show()
            linkCount++
        }
        if (giveawayModel?.isTwitterSelected!! && !giveawayModel?.twitterLink.isNullOrEmpty()) {
            cvTwitter?.show()
            linkCount++
        }
        if (giveawayModel?.isTiktokSelected!! && !giveawayModel?.tiktokLink.isNullOrEmpty()) {
            cvTikTok?.show()
            linkCount++
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSubmitEntries -> {
                submitEntryListener.onSubmitEntry()
            }
            R.id.clFacebook -> {
                openFacebook()
            }
            R.id.clInstagram -> {
                openPromotionLink(INSTAGRAM_REQUEST_CODE,giveawayModel?.instagramLink)
            }
            R.id.clTwitter -> {
                openPromotionLink(TWITTER_REQUEST_CODE,giveawayModel?.twitterLink)
            }
            R.id.clYoutube -> {
                openPromotionLink(YOUTUBE_REQUEST_CODE,giveawayModel?.youtubeLink)
            }
            R.id.clTikTok -> {
                openPromotionLink(TIKTOK_REQUEST_CODE,giveawayModel?.tiktokLink)
            }
            else -> dismiss()
        }
    }


    private fun openFacebook(){
        var pageId: String?= ""
        val facebookLink = giveawayModel?.facebookLink?.split("/")
        if(!facebookLink.isNullOrEmpty()){
            pageId = facebookLink[facebookLink.size - 1]
        }
        val pageUrl = "https://www.facebook.com/$pageId"
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW

        try {
            val applicationInfo: ApplicationInfo =
                requireContext().packageManager.getApplicationInfo("com.facebook.katana", 0)
            if (applicationInfo.enabled) {
                val versionCode =
                    requireContext().packageManager.getPackageInfo("com.facebook.katana", 0).versionCode
                val url: String
                url = if (versionCode >= 3002850) {
                    "fb://facewebmodal/f?href=$pageUrl"
                } else {
                    "fb://page/$pageId"
                }
                intent.data = Uri.parse(url)
                startActivityForResult(intent, FACEBOOK_REQUEST_CODE)
            } else {
                throw java.lang.Exception("Facebook is disabled")
            }
        } catch (e: java.lang.Exception) {
            intent.data = Uri.parse(pageUrl)
            startActivityForResult(intent, FACEBOOK_REQUEST_CODE)
        }
    }
    private fun openPromotionLink(requestCode: Int, pageLink: String?){
        try {
            val intent: Intent? = Intent()
            intent?.data = Uri.parse(pageLink)
            intent?.action = Intent.ACTION_VIEW
            startActivityForResult(intent, requestCode)
        } catch (e: ActivityNotFoundException) {
            Logger.error(e.message.toString())
        }
    }

    private fun setViewSelected(view: View){
        view.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.status_text_color
            )
        )
        view.disable()
        view.isClickable = false
        linkCount--
        if (linkCount == 0) {
             btnSubmitEntries?.isEnabled = true
             btnSubmitEntries?.setBackgroundResource(R.drawable.shape_common_button_bg)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            when (requestCode) {
                FACEBOOK_REQUEST_CODE -> {
                    showAlert(getString(R.string.page_like_question), clFacebook)
                }
                INSTAGRAM_REQUEST_CODE -> {
                    showAlert(getString(R.string.page_follow_question), clInstagram)
                }
                YOUTUBE_REQUEST_CODE -> {
                    showAlert(getString(R.string.page_subscribe_question), clYoutube)
                }
                TWITTER_REQUEST_CODE -> {
                    showAlert(getString(R.string.page_follow_question), clTwitter)
                }
                TIKTOK_REQUEST_CODE -> {
                    showAlert(getString(R.string.page_follow_question), clTwitter)
                }
            }
    }
    private fun showAlert(message: String, view: View){
        object : AppAlertDialog(requireContext()) {
            override fun onPositiveButton() {
                super.onPositiveButton()
                setViewSelected(view)
            }

            override fun onNegativeButton() {
                super.onNegativeButton()
                Logger.debug("Negative")
            }
        }.apply {
            setTitle("Confirmation")
            setButtonText("YES","NO")
            setMessage(message)
            showNegativeButton()
            show()
        }
    }

    interface SubmitEntryListener{
        fun onSubmitEntry()
    }
}

