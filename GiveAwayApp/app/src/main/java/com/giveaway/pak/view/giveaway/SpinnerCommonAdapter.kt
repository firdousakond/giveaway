package com.giveaway.pak.view.giveaway

import android.content.Context
import android.widget.ArrayAdapter

class SpinnerCommonAdapter <T>(context: Context, layoutId: Int, items: MutableList<T>) : ArrayAdapter<T>(context, layoutId, items) {

    override fun isEnabled(position: Int): Boolean {
        return position != 0
    }
}