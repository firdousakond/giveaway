package com.giveaway.pak.view.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.giveaway.pak.R


class AppProgressDialog : DialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Translucent_NoTitleBar)
        isCancelable = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_app_progress, container, false)
    }

    override fun show(manager: FragmentManager, tag: String?) {

        try {
            val ft = manager.beginTransaction()
            ft.add(this, tag)
            ft.commitAllowingStateLoss()
        } catch (e: IllegalStateException) {
            com.giveaway.pak.util.Logger.error(e.localizedMessage)
        }

    }

    companion object {

        private const val ARG_1 = "message"

        fun showProgressDialog(manager: FragmentManager?): AppProgressDialog {
            val progressDialog = AppProgressDialog()

            val bundle = Bundle()
            bundle.putString(ARG_1, "Please wait...")
            progressDialog.arguments = bundle
            manager?.let { progressDialog.show(it, "dialog") }

            return progressDialog
        }

    }
}
