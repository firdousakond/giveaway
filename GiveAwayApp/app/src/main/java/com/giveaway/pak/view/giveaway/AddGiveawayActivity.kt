package com.giveaway.pak.view.giveaway

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.CheckBox
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.giveaway.pak.BR
import com.giveaway.pak.R
import com.giveaway.pak.databinding.ActivityAddGiveawayBinding
import com.giveaway.pak.model.CityModel
import com.giveaway.pak.model.ContactDetails
import com.giveaway.pak.model.GiveawayModel
import com.giveaway.pak.util.*
import com.giveaway.pak.util.CommonUtil.displaySnackbar
import com.giveaway.pak.util.CommonUtil.isNetworkAvailable
import com.giveaway.pak.util.CommonUtil.isValidEmail
import com.giveaway.pak.view.base.BaseBindingActivity
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.dialog.AppAlertDialog
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_add_giveaway.*
import kotlinx.android.synthetic.main.activity_toolbar.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


class AddGiveawayActivity :
    BaseBindingActivity<ActivityAddGiveawayBinding, GiveawayViewModel, BaseHandler>(),
    ImageAdapter.UploadDocumentsDelegate {

    companion object {
        const val IMAGE_PICKER_CODE = 100
        const val STORAGE_PERMISSION_CODE = 101

    }

    @Inject
    lateinit var giveawayViewModel: GiveawayViewModel

    override fun getViewModel(): GiveawayViewModel? = giveawayViewModel

    override fun getBindingVariable(): Int = BR.viewmodel

    override fun getLayoutId(): Int = R.layout.activity_add_giveaway

    private lateinit var imageAdapter: ImageAdapter
    private val imageList = ArrayList<Uri>()
    private val uploadImageUrl = ArrayList<String>()
    private var platformCount = 0
    private var userId = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userId = SharedPrefUtil(this).getUserId()!!
        tvHeader.text = getString(R.string.add_giveaway)
        ivBack.show()
        ivAdd.hide()
        initObservers()
        setCities()
        setListener()
        setAdapter()
    }

    private fun initObservers() {
        giveawayViewModel.addGiveawayLiveData.observe(this, addGiveawayObserver)
        giveawayViewModel.imageUrlLiveData.observe(this, imageUriObserver)

    }

    private val addGiveawayObserver = Observer<LiveDataResult<Boolean>> { result ->
        when (result.status) {
            LiveDataResult.Status.LOADING -> {
                Logger.debug("Loading")
            }
            LiveDataResult.Status.SUCCESS -> {
                onDismissLoading()
                displaySnackbar(
                    this,
                    getString(R.string.success_msg_add_giveaway),
                    MessageType.SUCCESS
                )
                Handler().postDelayed({ finish() }, 2000)
            }
            LiveDataResult.Status.ERROR -> {
                onDismissLoading()
                displaySnackbar(
                    this,
                    getString(R.string.common_error_msg),
                    MessageType.ERROR
                )
            }

        }

    }


    private val imageUriObserver = Observer<LiveDataResult<Uri>> { result ->
        when (result.status) {
            LiveDataResult.Status.LOADING -> {
                onShowLoading()
            }
            LiveDataResult.Status.SUCCESS -> {
                uploadImageUrl.add(result.data.toString())
                imageList.removeAt(0)
                if (imageList.isNotEmpty()) {
                    giveawayViewModel.uploadFile(imageList[0])
                } else {
                    submitData()
                }
            }
            LiveDataResult.Status.ERROR -> {
                displaySnackbar(
                    this,
                    getString(R.string.common_error_msg),
                    MessageType.ERROR
                )
            }

        }

    }

    private fun setAdapter() {
        imageAdapter = ImageAdapter(this, this)
        rvPhoto.initializeHorizontal(imageAdapter)
    }

    private fun setCities() {

        val cityList = resources.openRawResource(R.raw.cities)
            .bufferedReader().use { it.readText() }
        val cityModel = Gson().fromJson(cityList, CityModel::class.java)
        val cities: MutableList<String> = cityModel.cities as MutableList<String>
        val adapter = SpinnerCommonAdapter(
            this, R.layout.item_common_dialog,
            cities
        )

        spCity.adapter = adapter
        spCity.setTitle(getString(R.string.select_city))
        spCity.setPositiveButton("CLOSE")
        spCity.setPadding(0, 0, 0, 0)
        val days = resources.getStringArray(R.array.days_list)
        val daysAdapter = SpinnerCommonAdapter(
            this, R.layout.item_common_dialog,
            days.toMutableList()
        )
        spDays.adapter = daysAdapter

    }

    private fun setListener() {

        cbFacebook.setOnCheckedChangeListener { _, isChecked ->
            showHideFacebook(isChecked)
        }
        cbInstagram.setOnCheckedChangeListener { _, isChecked ->
            showHideInstagram(isChecked)
        }
        cbTwitter.setOnCheckedChangeListener { _, isChecked ->
            showHideTwitter(isChecked)
        }
        cbYoutube.setOnCheckedChangeListener { _, isChecked ->
            showHideYoutube(isChecked)
        }
        cbTikTok.setOnCheckedChangeListener { _, isChecked ->
            showHideTikTok(isChecked)
        }

        clImages.setOnClickListener {
            checkStoragePermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                STORAGE_PERMISSION_CODE
            )
        }
        btnUploadNow.setOnClickListener {
            uploadImage()
        }

        etTitle.addTextChangedListener(TextChangeListener(etTitle))
        etNumWinners.addTextChangedListener(TextChangeListener(etNumWinners))
        etDescription.addTextChangedListener(TextChangeListener(etDescription))
        etEmail.addTextChangedListener(TextChangeListener(etEmail))
        etMobile.addTextChangedListener(TextChangeListener(etMobile))
        spCity.onItemSelectedListener = ItemSelectedListener()
        spDays.onItemSelectedListener = ItemSelectedListener()
    }


    private inner class ItemSelectedListener : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            if(parent == spCity){
                tvErrorCity.hide()
            }else if(parent == spDays){
                tvErrorDays.hide()
            }
        }

    }

    private fun uploadImage() {
        if (isValidField() && isNetworkAvailable(this)) {
            giveawayViewModel.uploadFile(imageList[0])
        }
    }

    private fun isValidField(): Boolean {

        var isValid: Boolean? = true

        if (etTitle.text.isNullOrEmpty()) {
            tvErrorTitle.show()
            isValid = false
        }

        if (etNumWinners.text.isNullOrEmpty()) {
            tvErrorWinners.show()
            isValid = false
        }


        if (etDescription.text.isNullOrEmpty()) {
            tvErrorDescription.show()
            isValid = false
        }
        if (etEmail.text.isNullOrEmpty() || !isValidEmail(etEmail.text.toString())) {
            tvErrorEmail.show()
            isValid = false
        }

        if (etMobile.text.isNullOrEmpty()) {
           tvErrorMobile.show()
            isValid = false
        }

        if (spCity.selectedItem == null || spCity.selectedItem.toString().isEmpty()) {
           tvErrorCity.show()
            isValid = false
        }
        if (spDays.selectedItem == null || spDays.selectedItemPosition == 0) {
            tvErrorDays.show()
            isValid = false
        }

        if (imageList.isNullOrEmpty()) {
            tvErrorImage.show()
            isValid =  false
        }

        if (platformCount == 0) {
            showAlertDialog(getString(R.string.error_msg_promotion_link))
            return false
        }

        if (isPromotionLinkEntered().isNotEmpty()) {
            showAlertDialog(isPromotionLinkEntered())
            return false
        }

        return isValid!!
    }

    private fun isPromotionLinkEntered(): String {
        if (cbFacebook.isChecked && etFacebookLink.text.isNullOrEmpty()) {
            return getString(R.string.error_facebook_link)
        }
        if (cbTwitter.isChecked && etTwitterLink.text.isNullOrEmpty()) {
            return getString(R.string.error_twitter_link)
        }
        if (cbYoutube.isChecked && etYoutubeLink.text.isNullOrEmpty()) {
            return getString(R.string.error_youtube_link)
        }
        if (cbInstagram.isChecked && etInstagramLink.text.isNullOrEmpty()) {
            return getString(R.string.error_instagram_link)
        }
        if (cbTikTok.isChecked && etTikTokLink.text.isNullOrEmpty()) {
            return getString(R.string.error_tiktok_link)
        }
        return ""
    }

    private fun submitData() {
        val currentLocalTime = Calendar.getInstance().timeInMillis
        val cal = Calendar.getInstance()
        val offset = cal.timeZone.getOffset(currentLocalTime)
        val utcTime =  currentLocalTime + offset
        val documentId = UUID.randomUUID().toString().toUpperCase(Locale.ENGLISH)
        val title = etTitle.text.toString()
        val city = spCity.selectedItem.toString()
        val days = spDays.selectedItem.toString()
        val winners = etNumWinners.text.toString()
        val description = etDescription.text.toString()
        val email = etEmail.text.toString()
        val mobile = etMobile.text.toString()
        val officeNumber = etTelephone.text.toString()
        val facebookLink = etFacebookLink.text.toString()
        val twitterLink = etTwitterLink.text.toString()
        val instagramLink = etInstagramLink.text.toString()
        val youtubeLink = etYoutubeLink.text.toString()
        val tiktokLink = etTikTokLink.text.toString()
        val contactDetails = ContactDetails()
        contactDetails.email = email
        contactDetails.mobileNumber = "+92 $mobile"
        contactDetails.officeNumber = "+92 $officeNumber"
        val isFacebookSelected = cbFacebook.isChecked
        val isYoutubeSelected = cbYoutube.isChecked
        val isInstagramSelected = cbInstagram.isChecked
        val isTwitterSelected = cbTwitter.isChecked
        val isTiktokSelected = cbTikTok.isChecked

        val giveawayModel = GiveawayModel()
        giveawayModel.id = documentId
        giveawayModel.userId = userId
        giveawayModel.city = city
        giveawayModel.contactDetails = contactDetails
        giveawayModel.createdDate = utcTime
        giveawayModel.title = title
        giveawayModel.description = description
        giveawayModel.numberOfDays = days
        giveawayModel.numberOfWinners = winners.toInt()
        giveawayModel.facebookLink = facebookLink
        giveawayModel.instagramLink = instagramLink
        giveawayModel.youtubeLink = youtubeLink
        giveawayModel.twitterLink = twitterLink
        giveawayModel.tiktokLink = tiktokLink
        giveawayModel.isFacebookSelected = isFacebookSelected
        giveawayModel.isTwitterSelected = isTwitterSelected
        giveawayModel.isYoutubeSelected = isYoutubeSelected
        giveawayModel.isInstagramSelected = isInstagramSelected
        giveawayModel.isTiktokSelected = isTiktokSelected
        giveawayModel.imageURLs = uploadImageUrl
        giveawayViewModel.addGiveaway(giveawayModel)

    }


    private fun isMaxLink(checkbox: CheckBox): Boolean {
        if (platformCount == 2) {
            toastMessage(getString(R.string.error_msg_promotion_link_max))
            checkbox.isChecked = false
            return true
        }
        return false
    }

    private fun showHideFacebook(checked: Boolean) {
        if (checked) {
            if (isMaxLink(cbFacebook)) {
                return
            }
            etFacebookLink.show()
            platformCount++
        } else {
            etFacebookLink.hide()
            etFacebookLink.text?.clear()
            platformCount--
        }
    }

    private fun showHideInstagram(checked: Boolean) {
        if (checked) {
            if (isMaxLink(cbInstagram)) {
                return
            }
            etInstagramLink.show()
            platformCount++
        } else {
            etInstagramLink.hide()
            etInstagramLink.text?.clear()
            platformCount--
        }
    }

    private fun showHideYoutube(checked: Boolean) {
        if (checked) {
            if (isMaxLink(cbYoutube)) {
                return
            }
            etYoutubeLink.show()
            platformCount++
        } else {
            etYoutubeLink.hide()
            etYoutubeLink.text?.clear()
            platformCount--
        }
    }

    private fun showHideTikTok(checked: Boolean) {
        if (checked) {
            if (isMaxLink(cbTikTok)) {
                return
            }
            etTikTokLink.show()
            platformCount++
        } else {
            etTikTokLink.hide()
            etTikTokLink.text?.clear()
            platformCount--
        }
    }

    private fun showHideTwitter(checked: Boolean) {
        if (checked) {
            if (isMaxLink(cbTwitter)) {
                return
            }
            etTwitterLink.show()
            platformCount++
        } else {
            etTwitterLink.hide()
            etTwitterLink.text?.clear()
            platformCount--
        }
    }


    override fun onImageRemoved(position: Int) {
        imageList.removeAt(position)
        setImageVisibility()
    }

    override fun onImageAdded() {
        setImageVisibility()
    }

    private fun setImageVisibility() {
        val imgSize = imageAdapter.itemCount

        if (imgSize >= IMAGE_LIMIT) {
            clImages.hide()
        } else {
            clImages.show()
        }
    }

    private fun pickImageFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_PICKER_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICKER_CODE) {

            if (data != null && data.data != null && data.clipData == null) {
                imageList.add(data.data!!)
                imageAdapter.deleteAll()
                imageAdapter.addItem(imageList)
                tvErrorImage.hide()
            } else if (data != null && data.clipData != null && data.clipData?.itemCount != null) {
                for (i in 0 until data.clipData?.itemCount!!) {
                    imageList.add(data.clipData!!.getItemAt(i).uri)
                }
                imageAdapter.deleteAll()
                imageAdapter.addItem(imageList)
                tvErrorImage.hide()
            }
        }

    }

    private fun checkStoragePermission(permission: String, requestCode: Int) {

        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(
                this,
                permission
            )
            == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat
                .requestPermissions(
                    this, arrayOf(permission),
                    requestCode
                )
        } else {
            pickImageFromGallery()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                pickImageFromGallery()
            } else {
                Toast.makeText(
                    this,
                    "Storage Permission Denied",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
        }
    }

    fun showAlertDialog(message: String) {
        object : AppAlertDialog(this@AddGiveawayActivity) {
            override fun onPositiveButton() {
                super.onPositiveButton()
                Logger.debug("OK")
            }
        }.apply {
            setMessage(message)
            show()
        }
    }



    private inner class TextChangeListener(val view: View) : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            showHideError(s.toString())
        }

        private fun showHideError(text: String) {
            when(view){
                etTitle -> {
                    if (text.isEmpty()) {
                        tvErrorTitle.show()
                    } else {
                        tvErrorTitle.hide()
                    }
                }
                etNumWinners -> {
                    if (text.isEmpty()) {
                        tvErrorWinners.show()
                    } else {
                        tvErrorWinners.hide()
                    }
                }
                etDescription -> {
                    if (text.isEmpty()) {
                        tvErrorDescription.show()
                    } else {
                        tvErrorDescription.hide()
                    }
                }
                etEmail -> {
                    if (text.isEmpty()) {
                        tvErrorEmail.show()
                    } else {
                        tvErrorEmail.hide()
                    }
                }
                etMobile -> {
                    if (text.isEmpty()) {
                        tvErrorMobile.show()
                    } else {
                        tvErrorMobile.hide()
                    }
                }
            }
        }
    }

}