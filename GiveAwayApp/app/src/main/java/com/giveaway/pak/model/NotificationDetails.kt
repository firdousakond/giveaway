package com.giveaway.pak.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationDetails(var title: String? = null, var message: String? = null) : Parcelable