package com.giveaway.pak.view.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.giveaway.pak.R
import com.giveaway.pak.util.Logger
import com.giveaway.pak.view.dialog.AppProgressDialog
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_toolbar.*
import java.lang.reflect.ParameterizedType
import java.util.*
import javax.inject.Inject


abstract class BaseBindingActivity<B : ViewDataBinding, VM : BaseViewModel<N>, N : BaseHandler> : AppCompatActivity(),
    BaseHandler, HasAndroidInjector {
    private val noViewModelBindingVariable = -1
    var dataBinding: B? = null
    lateinit var binding: B

    private var mViewModel: VM? = null
    private var appProgress: AppProgressDialog? = null
    open fun initComponents() {}
    abstract fun getViewModel(): VM?
    abstract fun getBindingVariable(): Int
    abstract fun getLayoutId(): Int
    private lateinit var manager: FragmentManager

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var mFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        manager  = supportFragmentManager
        initComponents()
        provideViewModel()
        performDataBinding()
        initToolbar(tool_bar)
    }

    private fun initToolbar(toolbar: Toolbar?) {
        if (toolbar == null) {
            return
        }
        setSupportActionBar(toolbar)
        ivBack.setOnClickListener { finish() }
    }

    private fun performDataBinding() {
        dataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        this.mViewModel = if (mViewModel == null) getViewModel() else mViewModel
        if (getBindingVariable() != noViewModelBindingVariable) {
            dataBinding?.setVariable(getBindingVariable(), mViewModel)
            dataBinding?.executePendingBindings()
        }
        binding = dataBinding!!
    }

    private fun provideViewModel() {
        val clazz: Class<VM> = getViewModelClass(javaClass)
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(clazz)
    }

    private fun getViewModelClass(aClass: Class<*>): Class<VM> {
        val type = aClass.genericSuperclass

        return if (type is ParameterizedType) {
            type.actualTypeArguments[1] as Class<VM>
        } else {
            getViewModelClass(aClass.superclass as Class<*>)
        }
    }


    override fun onShowLoading(message: String) {
        if (appProgress != null) {
            if (appProgress?.isVisible!!) {
                return
            }
            return
        }

        appProgress = AppProgressDialog.showProgressDialog(supportFragmentManager)
    }

    override fun onDismissLoading() {
         if (appProgress != null && appProgress?.isVisible!!) {
            appProgress?.dismissAllowingStateLoss()
            appProgress = null
        }
    }

    override fun onError(errorCode: String) {

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right)
    }
    fun proceedToHome() {
        super.onBackPressed()
        finish()
    }


    /**
     * Returns the number of fragments in the stack.
     *
     * @return the number of fragments in the stack.
     */
    open fun size(): Int {
        return getFragments().size
    }

    /**
     * Pushes a fragment to the top of the stack.
     *
     * @param <T>           the type parameter
     * @param fragmentClass the fragment class
     * @param bundle        the bundle
    </T> */
    open fun <T : Fragment?> add(
        fragmentClass: Class<T>,
        bundle: Bundle?
    ) {
        val top = peek()
        var fragment: Fragment? = manager.findFragmentByTag(
            fragmentClass.simpleName
        )
        if (fragment == null) {
            try {
                fragment = fragmentClass.newInstance()
                fragment!!.arguments = bundle
            } catch (e: Exception) {
                Logger.error(e.message.toString())
            }
        }
        if (fragment != null) {
            if (top != null) {
                manager.beginTransaction().hide(top)
                    .add(R.id.frameLayoutHost, fragment, indexToTag(manager.backStackEntryCount + 1))
                    .addToBackStack(null)
                    .commitAllowingStateLoss()
            } else {
                manager.beginTransaction()
                    .add(R.id.frameLayoutHost, fragment, indexToTag(0))
                    .commitAllowingStateLoss()
            }
            manager.executePendingTransactions()
        }
    }

    open fun <T : Fragment?> replace(
        fragmentClass: Class<T>,
        bundle: Bundle?
    ) {
        var fragment: Fragment? = manager.findFragmentByTag(
            fragmentClass.simpleName
        )
        if (fragment == null) {
            try {
                fragment = fragmentClass.newInstance()
                fragment!!.arguments = bundle
            } catch (e: Exception) {
                Logger.error(e.message.toString())
            }
        }
        if (fragment != null) {
            if (manager.backStackEntryCount != 0) {
                manager.popBackStackImmediate(
                    null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE
                )
            }
            manager.beginTransaction()
                .replace(R.id.frameLayoutHost, fragment, indexToTag(0))
                .commitAllowingStateLoss()
            manager.executePendingTransactions()
        }
    }

    /**
     * Returns the topmost fragment in the stack.
     *
     * @return the fragment
     */
    open fun peek(): Fragment? {
        return manager.findFragmentById(R.id.frameLayoutHost)
    }

    open fun <T> findCallback(
        fragment: Fragment,
        callbackType: Class<T>
    ): T? {
        val back = getBackFragment(fragment)
        if (back != null && callbackType.isAssignableFrom(back.javaClass)) {
            return back as T
        }
        return if (callbackType.isAssignableFrom(this.javaClass)) {
            this as T
        } else null
    }

     fun getBackFragment(fragment: Fragment): Fragment? {
        val fragments = getFragments()
        for (f in fragments.indices.reversed()) {
            if (fragments[f] === fragment && f > 0) {
                return fragments[f - 1]
            }
        }
        return null
    }

    protected open fun getFragments(): List<Fragment> {
        val fragments: MutableList<Fragment> =
            ArrayList<Fragment>(manager.backStackEntryCount + 1)
        for (i in 0 until manager.backStackEntryCount + 1) {
            val fragment: Fragment? = manager.findFragmentByTag(indexToTag(i))
            if (fragment != null) {
                fragments.add(fragment)
            }
        }
        return fragments
    }

    fun indexToTag(index: Int): String? {
        return Integer.toString(index)
    }

}