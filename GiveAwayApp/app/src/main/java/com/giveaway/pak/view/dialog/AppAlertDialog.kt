package com.giveaway.pak.view.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Spanned
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.annotation.StringRes
import com.giveaway.pak.R
import java.util.*


abstract class AppAlertDialog : Dialog, View.OnClickListener {

    private var btnOk: TextView? = null
    private var btnCancel: TextView? = null
    private var textTitle: TextView? = null
    private var textMessage: TextView? = null

    private var title: String? = null
    private var positiveTxt: String = "OK"
    private var negativeTxt: String = "CANCEL"
    private var message: String? = null
    private var displayNegative = false
    private var alertMessageSpanned: Spanned? = null

    internal var listener: View.OnClickListener? = null

    constructor(context: Context?) : super(context!!)

    constructor(context: Context, listener: View.OnClickListener) : super(context) {
        this.listener = listener
    }

    open fun onPositiveButton() {}
    open fun onNegativeButton() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        setContentView(R.layout.dialog_app_alert)
        setCanceledOnTouchOutside(false)
        setCancelable(false)

        textTitle = findViewById(R.id.textTitle)
        textMessage = findViewById(R.id.textMessage)

        textTitle?.text = title?:context.getString(R.string.alert)

        textMessage?.text = message

        btnOk = findViewById(R.id.btnOk)
        btnOk?.text = positiveTxt

        btnOk?.setOnClickListener(this)

        btnCancel = findViewById(R.id.btnCancel)
        btnCancel?.text = negativeTxt
        btnCancel?.setOnClickListener(this)

        btnCancel?.visibility = if (displayNegative) View.VISIBLE else View.GONE

    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun setMessage(message: Spanned) {
        this.alertMessageSpanned = message
    }

    fun setButtonText(positiveTxt: String = "OK", negativeTxt: String = "CANCEL") {
        this.positiveTxt = positiveTxt.toUpperCase(Locale.ENGLISH)
        this.negativeTxt = negativeTxt.toUpperCase(Locale.ENGLISH)
    }

    fun setButtonText(@StringRes positiveTxt: Int = R.string.ok, @StringRes negativeTxt: Int = R.string.cancel) {
        this.positiveTxt = context.getString(positiveTxt).toUpperCase(Locale.ENGLISH)
        this.negativeTxt = context.getString(negativeTxt).toUpperCase(Locale.ENGLISH)
    }


    fun showNegativeButton() {
        displayNegative = true
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnOk -> {
                dismiss()
                onPositiveButton()
            }
            R.id.btnCancel -> {
                dismiss()
                onNegativeButton()
            }
        }
    }

    companion object {

        fun showAlertDialog(context: Context?, message: String?, title: String = "Alert"): AppAlertDialog {
            val alertDialog = object : AppAlertDialog(context) {

            }
            alertDialog.setMessage(message?:"")
            alertDialog.setTitle(title)
            alertDialog.setCanceledOnTouchOutside(true)
            alertDialog.show()
            return alertDialog
        }
    }
}

