package com.giveaway.pak.util


const val ACCESS_TOKEN = "access_token"
const val HAS_USER = "has_user"
const val REFRESH_TOKEN = "refresh_token"
const val FIRST_LAUNCH = "first_launch"
const val CONTENT_TYPE = "Content-Type"
const val APPLICATION_JSON = "application/json"
const val CONSTANT_GLIDE_TIMEOUT = 60000
const val IMAGE_LIMIT = 5
const val PAGE_LIMIT = 5L
const val GIVEAWAY_MODEL = "giveawayModel"
const val USER_ID = "USER_ID"
const val IS_MY_GIVEAWAY = "IS_MY_GIVEAWAY"
const val IS_MY_ENTRIES = "IS_MY_ENTRIES"
const val IS_WON_GIVEAWAY = "IS_WON_GIVEAWAY"



enum class MessageType {
    GENERAL,
    ERROR,
    SUCCESS
}
