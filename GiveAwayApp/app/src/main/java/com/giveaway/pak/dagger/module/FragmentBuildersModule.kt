package com.giveaway.pak.dagger.module

import com.giveaway.pak.view.announcement.AnnouncementFragment
import com.giveaway.pak.view.common.EnterWinDialog
import com.giveaway.pak.view.giveaway.GiveawayFragment
import com.giveaway.pak.view.settings.SettingsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeGiveawayFragment(): GiveawayFragment

    @ContributesAndroidInjector
    abstract fun contributeAnnouncementFragment(): AnnouncementFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingsFragment(): SettingsFragment

    @ContributesAndroidInjector
    abstract fun contributeEnterWinDialog(): EnterWinDialog
}