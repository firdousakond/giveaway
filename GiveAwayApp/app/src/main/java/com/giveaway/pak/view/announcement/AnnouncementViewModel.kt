package com.giveaway.pak.view.announcement

import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.base.BaseViewModel
import com.google.firebase.firestore.*
import com.google.firebase.storage.StorageReference
import javax.inject.Inject

class AnnouncementViewModel @Inject constructor(
    private var firestore: FirebaseFirestore,
    private var storageReference: StorageReference
) :
    BaseViewModel<BaseHandler>() {


}