package com.giveaway.pak.model

import android.os.Parcelable
import com.giveaway.pak.R
import com.giveaway.pak.view.login.LoginActivity
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WalkThroughModel(var title: String, var image: Int, var desc: String) : Parcelable {

    companion object {

        fun getDefaultWalkThrough(activity: LoginActivity): ArrayList<WalkThroughModel> {
            val walkThroughList = ArrayList<WalkThroughModel>()

            walkThroughList.add(
                WalkThroughModel(
                    activity.getString(R.string.participate_win_title),
                    R.drawable.giveaway_slide1,
                    activity.getString(R.string.participate_win_message)
                )
            )

            walkThroughList.add(
                WalkThroughModel(
                    activity.getString(R.string.promote_business_title),
                    R.drawable.giveaway_slide2,
                    activity.getString(R.string.promote_business_message)
                )
            )
            return walkThroughList
        }
    }

}
