package com.giveaway.pak.view.entry

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.giveaway.pak.BR
import com.giveaway.pak.R
import com.giveaway.pak.databinding.ActivityEntryBinding
import com.giveaway.pak.model.GiveawayModel
import com.giveaway.pak.util.*
import com.giveaway.pak.view.base.BaseBindingActivity
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.dialog.AppAlertDialog
import kotlinx.android.synthetic.main.activity_entry.*
import kotlinx.android.synthetic.main.activity_toolbar.*
import javax.inject.Inject


class EntryActivity : BaseBindingActivity<ActivityEntryBinding, EntryViewModel, BaseHandler>() {

    companion object{
        private const val FACEBOOK_REQUEST_CODE = 100
        private const val INSTAGRAM_REQUEST_CODE = 200
        private const val YOUTUBE_REQUEST_CODE = 300
        private const val TWITTER_REQUEST_CODE = 400
        private const val TIKTOK_REQUEST_CODE = 500

    }
    @Inject
    lateinit var entryViewModel: EntryViewModel
    override fun getViewModel(): EntryViewModel? = entryViewModel

    override fun getBindingVariable(): Int = BR.viewmodel

    override fun getLayoutId(): Int = R.layout.activity_entry
    private lateinit var giveawayModel: GiveawayModel
    private var linkCount = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tvHeader.text = getString(R.string.enter_win)
        ivBack.show()
        getBundle()
        setListener()
    }

    private fun setListener() {
        clFacebook.setOnClickListener { openFacebook() }
        clInstagram.setOnClickListener { openPromotionLink(INSTAGRAM_REQUEST_CODE,giveawayModel.instagramLink) }
        clYoutube.setOnClickListener { openPromotionLink(YOUTUBE_REQUEST_CODE,giveawayModel.youtubeLink) }
        clTwitter.setOnClickListener { openPromotionLink(TWITTER_REQUEST_CODE,giveawayModel.twitterLink) }
        clTiktok.setOnClickListener { openPromotionLink(TIKTOK_REQUEST_CODE,giveawayModel.tiktokLink) }
        btnSubmitEntry.setOnClickListener { submitEntry() }
    }

    private fun submitEntry() {
//        entryViewModel.addEntries(giveawayModel)
    }

    private fun getBundle() {
        val bundle = intent.extras
        if (bundle != null) {
            giveawayModel = bundle.getSerializable(GIVEAWAY_MODEL) as GiveawayModel
            setEntries()
        }
    }

    private fun setEntries() {
        if (giveawayModel.isFacebookSelected!! && !giveawayModel.facebookLink.isNullOrEmpty()) {
            cardFacebook.show()
            linkCount++
        }
        if (giveawayModel.isInstagramSelected!! && !giveawayModel.instagramLink.isNullOrEmpty()) {
            cardInstagram.show()
            linkCount++
        }
        if (giveawayModel.isYoutubeSelected!! && !giveawayModel.youtubeLink.isNullOrEmpty()) {
            cardYoutube.show()
            linkCount++
        }
        if (giveawayModel.isTwitterSelected!! && !giveawayModel.twitterLink.isNullOrEmpty()) {
            cardTwitter.show()
            linkCount++
        }
        if (giveawayModel.isTiktokSelected!! && !giveawayModel.tiktokLink.isNullOrEmpty()) {
            cardTiktok.show()
            linkCount++
        }
    }

    private fun openFacebook(){
        var pageId: String?= ""
        val facebookLink = giveawayModel.facebookLink?.split("/")
        if(!facebookLink.isNullOrEmpty()){
            pageId = facebookLink[facebookLink.size - 1]
        }
        val pageUrl = "https://www.facebook.com/$pageId"
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW

        try {
            val applicationInfo: ApplicationInfo =
                packageManager.getApplicationInfo("com.facebook.katana", 0)
            if (applicationInfo.enabled) {
                val versionCode =
                    packageManager.getPackageInfo("com.facebook.katana", 0).versionCode
                val url: String
                url = if (versionCode >= 3002850) {
                    "fb://facewebmodal/f?href=$pageUrl"
                } else {
                    "fb://page/$pageId"
                }
                 intent.data = Uri.parse(url)
                startActivityForResult(intent, FACEBOOK_REQUEST_CODE)
            } else {
                throw java.lang.Exception("Facebook is disabled")
            }
        } catch (e: java.lang.Exception) {
            intent.data = Uri.parse(pageUrl)
            startActivityForResult(intent, FACEBOOK_REQUEST_CODE)
        }
    }
    private fun openPromotionLink(requestCode: Int, pageLink: String?){
        try {
            val intent: Intent? = Intent()
            intent?.data = Uri.parse(pageLink)
            intent?.action = Intent.ACTION_VIEW
            startActivityForResult(intent, requestCode)
        } catch (e: ActivityNotFoundException) {
          Logger.error(e.message.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            FACEBOOK_REQUEST_CODE -> {showAlert(getString(R.string.page_like_question),clFacebook)}
            INSTAGRAM_REQUEST_CODE -> {showAlert(getString(R.string.page_follow_question),clInstagram)}
            YOUTUBE_REQUEST_CODE -> {showAlert(getString(R.string.page_subscribe_question),clYoutube)}
            TWITTER_REQUEST_CODE -> {showAlert(getString(R.string.page_follow_question),clTwitter)}
            TIKTOK_REQUEST_CODE -> {showAlert(getString(R.string.page_follow_question),clTiktok)}

        }
    }

    private fun showAlert(message: String, view: View){
        object : AppAlertDialog(this@EntryActivity) {
            override fun onPositiveButton() {
                super.onPositiveButton()
                view.setBackgroundColor(ContextCompat.getColor(this@EntryActivity, R.color.status_text_color))
                view.disable()
                view.isClickable = false
                linkCount--
                if(linkCount == 0){
                   enableButton()
                }
            }

            override fun onNegativeButton() {
                super.onNegativeButton()
                Logger.debug("Negative")
            }
        }.apply {
            setTitle("Confirmation")
            setButtonText("YES","NO")
            setMessage(message)
            showNegativeButton()
            show()
        }
    }

    private fun enableButton() {
        btnSubmitEntry.enable()
        btnSubmitEntry.setBackgroundResource(R.drawable.common_button_rounded_selector)
    }
}
