package com.giveaway.pak.view.base

interface BaseHandler {

        fun onShowLoading(message: String = "Please wait")
        fun onDismissLoading()
        fun onError(errorCode: String = "")

}