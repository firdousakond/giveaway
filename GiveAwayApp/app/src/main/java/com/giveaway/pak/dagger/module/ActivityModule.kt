package com.giveaway.pak.dagger.module

import com.giveaway.pak.view.common.FullImageActivity
import com.giveaway.pak.view.entry.EntryActivity
import com.giveaway.pak.view.giveaway.AddGiveawayActivity
import com.giveaway.pak.view.giveaway.GiveawayDetailsActivity
import com.giveaway.pak.view.home.HomeActivity
import com.giveaway.pak.view.login.LoginActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {


    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeHomeActivity() : HomeActivity

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeAddGiveawayActivity() : AddGiveawayActivity

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeGiveawayDetailsActivity() : GiveawayDetailsActivity

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeFullImageActivity() : FullImageActivity


    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeEntryActivity() : EntryActivity




}