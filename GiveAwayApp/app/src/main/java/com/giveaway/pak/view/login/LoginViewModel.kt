package com.giveaway.pak.view.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.giveaway.pak.model.UserModel
import com.giveaway.pak.util.LiveDataResult
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.base.BaseViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private var firestore: FirebaseFirestore
) : BaseViewModel<BaseHandler>() {

    var userLiveData : MutableLiveData<LiveDataResult<QuerySnapshot>> = MutableLiveData()
    var userDetailsLiveData : MutableLiveData<LiveDataResult<QuerySnapshot>> = MutableLiveData()

    fun registerUser(userModel: UserModel) {
        viewModelScope.launch(Dispatchers.IO) {

            userLiveData.postValue(LiveDataResult.loading())
            firestore.collection("user").document(userModel.id!!)
                .set(userModel)
                .addOnSuccessListener {
                    val docReference = firestore.collection("user").whereEqualTo("id",userModel.id)
                    docReference.get()
                        .addOnSuccessListener {
                        userLiveData.postValue(LiveDataResult.success(it))
                    }.addOnFailureListener {
                        userLiveData.postValue(LiveDataResult.error(it))
                    }
                }
                .addOnFailureListener {
                    userLiveData.postValue(LiveDataResult.error(it))
                }
        }
    }

    fun fetchUser(facebookId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            userDetailsLiveData.postValue(LiveDataResult.loading())
            val docReference = firestore.collection("user").whereEqualTo("facebookId", facebookId)
            docReference.get()
                .addOnSuccessListener {
                    userDetailsLiveData.postValue(LiveDataResult.success(it))
                }.addOnFailureListener {
                    userDetailsLiveData.postValue(LiveDataResult.error(it))
                }
            }
        }
}