package com.giveaway.pak.view.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.giveaway.pak.R
import com.giveaway.pak.model.WalkThroughModel
import kotlinx.android.synthetic.main.fragment_walkthrough.*


class WalkThroughFragment : Fragment() {

    private var tutorials = mutableListOf<WalkThroughModel>()
    private var position = -1

    fun newInstance(tutorialList: ArrayList<WalkThroughModel>, position: Int): WalkThroughFragment {
        val args = Bundle()
        args.putParcelableArrayList("tutorials", tutorialList)
        args.putInt("position", position)
        val fragment = WalkThroughFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        tutorials =
            arguments?.getParcelableArrayList<WalkThroughModel>("tutorials")?.toMutableList()!!
        position = arguments?.getInt("position")!!
        return inflater.inflate(R.layout.fragment_walkthrough, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ivImage.setImageResource(tutorials[position].image)
        tvTitle.text = tutorials[position].title
        tvDescription.text = tutorials[position].desc
    }

}