package com.giveaway.pak.model


data class UserModel(
    var id: String?=null,
    var facebookId: String?=null,
    var name : String?=null,
    var imageURL : String?=null,
    var email: String?=null
)