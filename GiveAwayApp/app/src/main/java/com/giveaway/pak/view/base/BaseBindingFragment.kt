package com.giveaway.pak.view.base

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.giveaway.pak.R
import com.giveaway.pak.dagger.Injectable
import com.giveaway.pak.view.dialog.AppProgressDialog
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import java.lang.reflect.ParameterizedType
import javax.inject.Inject


abstract class BaseBindingFragment<B : ViewDataBinding, VM : BaseViewModel<N>, N : BaseHandler> : Fragment(),
    Injectable, BaseHandler, HasAndroidInjector {
    val NO_VIEW_MODEL_BINDING_VARIABLE = -1
    var dataBinding: B? = null
    protected var mViewModel: VM? = null
    private var appProgress: AppProgressDialog? = null
    lateinit var binding: B
    private var snackBar: Snackbar? = null

    abstract fun getBindingVariable(): Int
    abstract fun getLayoutId(): Int
    abstract fun getViewModel(): VM?
    open fun initComponents() {}
    open fun initBindings() {}
    open fun loadData() {}



    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector


    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var mFragmentInjector: DispatchingAndroidInjector<Fragment>

    //private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        provideViewModel()
        performDataBinding(inflater, container)
       // remoteHandler = RemoteHandler(activity, this)
        initBindings()
        return dataBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initComponents()
    }

    private fun performDataBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) {
        dataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        this.mViewModel = if (mViewModel == null) getViewModel() else mViewModel
        binding = dataBinding!!

    }

    private fun provideViewModel() {
        val clazz: Class<VM> = getViewModelClass(javaClass)
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(clazz)
    }

    private fun getViewModelClass(aClass: Class<*>): Class<VM> {
        val type = aClass.genericSuperclass

        return if (type is ParameterizedType) {
            type.actualTypeArguments[1] as Class<VM>
        } else {
            getViewModelClass(aClass.superclass as Class<*>)
        }
    }


    override fun onShowLoading(message: String) {
        if (appProgress != null) {
            if (appProgress?.isVisible!!) {
                return
            }
            return
        }

        appProgress = AppProgressDialog.showProgressDialog(fragmentManager)
    }

    override fun onDismissLoading() {
        if (appProgress != null && appProgress?.isVisible!!) {
            appProgress?.dismissAllowingStateLoss()
        }
        appProgress = null
    }

    override fun onError(errorCode: String) {

    }

    fun showSnackBar(parentContainer: ViewGroup, onFailed: () -> Unit) {
        showSnackBar(parentContainer, "Something went wrong please try again", onFailed, Snackbar.LENGTH_INDEFINITE)
    }

    fun showSnackBar(parentContainer: ViewGroup, message: String, onFailed: () -> Unit, snackBarLength: Int) {
        if (snackBar != null && !snackBar?.isShown!!) {
            snackBar?.show()
            return
        }

        snackBar = Snackbar
            .make(parentContainer, message, snackBarLength)
            .apply {
                setActionTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
                    .setTextColor(ContextCompat.getColor(context, R.color.red_color))
                view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).textSize = 20F
                view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).textSize = 16F
                view.setPadding(0, 0, 0, 0)
                view.setBackgroundColor(Color.WHITE)
                setAction("RETRY") {

                    onFailed()
                    dismiss()
                    snackBar = null
                }
                show()
            }
    }


}