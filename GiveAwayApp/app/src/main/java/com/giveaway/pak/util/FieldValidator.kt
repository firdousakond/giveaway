package com.giveaway.pak.util

import android.text.SpannableStringBuilder
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout

class FieldValidator {

    companion object {

        fun isValidEmail(target: CharSequence?): Boolean {
            return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        fun hasInvalidField(textInputs: MutableList<EditText?>): Boolean {
            var hasInvalidField = false
            for (textInput in textInputs) {
                if (!textInput?.error.isNullOrBlank() || textInput?.text.isNullOrBlank()) {
                    hasInvalidField = true
                }
            }
            return hasInvalidField
        }

        fun validateFields(textInputs: MutableList<TextInputLayout>, message: String): Boolean {
            var isValid = true
            for (textInput in textInputs) {
                if (textInput.editText?.text?.isBlank()!!) {
                    textInput.isErrorEnabled = true
                    textInput.error = message
                    isValid = false
                }
            }
            return isValid
        }

        fun convertNullsToString(textInputs: MutableList<TextInputLayout>) {
            for (textInput in textInputs) {
                if (textInput.editText?.text == null) {
                    textInput.editText?.text = SpannableStringBuilder("")
                }
            }
        }

        fun hasOneAnsweredField(textInputs: MutableList<TextInputLayout>, message: String): Boolean {
            var isValid = false
            for (textInput in textInputs) {
                if (!textInput.editText?.text?.isBlank()!!) {
                    isValid = true
                }
            }
            return isValid
        }
    }

}