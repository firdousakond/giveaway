package com.giveaway.pak.dagger.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.giveaway.pak.dagger.ViewModelKey
import com.giveaway.pak.dagger.GiveawayViewModelFactory
import com.giveaway.pak.view.announcement.AnnouncementViewModel
import com.giveaway.pak.view.entry.EntryViewModel
import com.giveaway.pak.view.giveaway.GiveawayViewModel
import com.giveaway.pak.view.home.HomeViewModel
import com.giveaway.pak.view.login.LoginViewModel
import com.giveaway.pak.view.settings.SettingsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: GiveawayViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun loginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun homeViewModel(homeViewModel: HomeViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GiveawayViewModel::class)
    abstract fun giveawayViewModel(giveawayViewModel: GiveawayViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EntryViewModel::class)
    abstract fun entryViewModel(entryViewModel: EntryViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AnnouncementViewModel::class)
    abstract fun announcementViewModel(announcementViewModel: AnnouncementViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    abstract fun settingsViewModel(settingsViewModel: SettingsViewModel) : ViewModel

}
