package com.giveaway.pak.view.giveaway

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.giveaway.pak.BR
import com.giveaway.pak.R
import com.giveaway.pak.databinding.FragmentGiveawaysBinding
import com.giveaway.pak.model.GiveawayModel
import com.giveaway.pak.util.*
import com.giveaway.pak.util.CommonUtil.isNetworkAvailable
import com.giveaway.pak.view.base.BaseBindingFragment
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.home.HomeActivity
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_giveaway_details.*
import kotlinx.android.synthetic.main.fragment_giveaways.*
import kotlinx.android.synthetic.main.item_no_giveaway.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class GiveawayFragment :
    BaseBindingFragment<FragmentGiveawaysBinding, GiveawayViewModel, BaseHandler>() {

    @Inject
    lateinit var giveawayViewModel: GiveawayViewModel

    override fun getBindingVariable(): Int = BR.viewmodel

    override fun getLayoutId(): Int = R.layout.fragment_giveaways

    override fun getViewModel(): GiveawayViewModel? = giveawayViewModel

    private lateinit var adapter: GiveawayAdapter
    private val giveawayList: ArrayList<GiveawayModel> = ArrayList()
    private val filteredGiveawayList: ArrayList<GiveawayModel> = ArrayList()
    private var userId = ""
    private var isMyEntries: Boolean? = false
    private var isMyGiveaway: Boolean? = false

    companion object {
        fun newInstance(): GiveawayFragment {
            return GiveawayFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userId = SharedPrefUtil(requireContext()).getUserId()!!
        initBindings()
        initComponents()
        initObserver()
        setListener()
        getBundle()
    }

    private fun getBundle() {
        if (arguments != null) {
            isMyEntries = arguments?.getBoolean(IS_MY_ENTRIES, false)
            isMyGiveaway = arguments?.getBoolean(IS_MY_GIVEAWAY, false)
            if (isMyGiveaway!!) {
                fetchMyGiveaway()
            } else {
                fetchGiveaway()
            }
        } else {
            fetchGiveaway()
        }
    }

    private fun initObserver() {
        giveawayViewModel.giveawayLiveData.observe(viewLifecycleOwner, giveawayObserver)
    }


    private val giveawayObserver = Observer<LiveDataResult<QuerySnapshot>> { result ->
        when (result.status) {
            LiveDataResult.Status.LOADING -> {
                onShowLoading()
            }
            LiveDataResult.Status.SUCCESS -> {
                onDismissLoading()
                swipeRefresh.isRefreshing = false
                if (result.data != null) {
                    adapter.deleteAll()
                    val giveawayList: ArrayList<GiveawayModel> = ArrayList()
                    for (documentSnapshot in result.data.documents) {
                        giveawayList.add(documentSnapshot.toObject(GiveawayModel::class.java)!!)
                    }
                    this.giveawayList.addAll(giveawayList)
                    when {
                        this.giveawayList.isNullOrEmpty() -> {
                            layoutNoGiveaway.show()
                        }
                        isMyEntries!! -> {
                            val myGiveaway: MutableList<GiveawayModel> = ArrayList()
                            giveawayList.forEach {
                                it.entries?.forEach { entries ->
                                    if (entries.uid == userId) {
                                        myGiveaway.add(it)
                                    }
                                }
                            }
                            if (myGiveaway.isEmpty()) {
                                layoutNoGiveaway.show()
                            } else {
                                adapter.addAll(myGiveaway)
                            }
                        }
                        else -> {
                            adapter.addAll(giveawayList)
                        }
                    }

                }
            }
            LiveDataResult.Status.ERROR -> {
                swipeRefresh.isRefreshing = false
                onDismissLoading()
                CommonUtil.displaySnackbar(
                    activity,
                    getString(R.string.common_error_msg),
                    MessageType.ERROR
                )
            }

        }

    }

    private fun setListener() {
        etSearch.addTextChangedListener(textChangeListener)
        btnTryAgain.setOnClickListener { fetchGiveaway() }
        swipeRefresh.setOnRefreshListener {
            if (isMyGiveaway!!) {
                fetchMyGiveaway()
            } else {
                fetchGiveaway()
            }
        }
    }

    private val textChangeListener = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            Logger.debug(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            Logger.debug(s.toString())
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            adapter.deleteAll()
            if (s.isNullOrEmpty()) {
                adapter.addAll(giveawayList)
            } else {
                setFilteredGiveaway(s.toString().toLowerCase(Locale.ENGLISH))
            }
        }

        private fun setFilteredGiveaway(searchKey: String) {
            filteredGiveawayList.clear()
            giveawayList.forEach {
                if (it.title?.toLowerCase(Locale.ENGLISH)
                        ?.contains(searchKey)!! || it.city?.toLowerCase(Locale.ENGLISH)
                        ?.contains(searchKey)!!
                ) {
                    filteredGiveawayList.add(it)
                }
            }
            adapter.addAll(filteredGiveawayList)
        }
    }

    private fun setRecyclerData() {
        val layoutManager = GridLayoutManager(activity, 2, RecyclerView.VERTICAL, false)
        rvGiveaways.layoutManager = layoutManager
        rvGiveaways.adapter = adapter
        fetchGiveaway()
        var loading = true
        var pastVisiblesItems: Int
        var visibleItemCount: Int
        var totalItemCount: Int
        rvGiveaways.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) { //check for scroll down
                    visibleItemCount = layoutManager.childCount
                    totalItemCount = layoutManager.itemCount
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false
                            fetchGiveaway()
                        }
                    }
                }
            }


        })

    }

    private fun fetchMyEntries() {

    }

    private fun fetchMyGiveaway() {
        if (isNetworkAvailable(requireActivity())) {
            giveawayViewModel.fetchMyGiveaway(userId)
        }
    }

    private fun fetchGiveaway() {
        if (isNetworkAvailable(requireActivity())) {
            giveawayViewModel.fetchGiveaway()
        }
    }

    override fun initBindings() {
        super.initBindings()
        dataBinding?.handler = this
    }

    override fun initComponents() {
        super.initComponents()
        if (activity is HomeActivity) {
            (activity as HomeActivity).setToolbarTitle(getString(R.string.giveaways))
            (activity as HomeActivity).hideLogoutIcon()

        }
        swipeRefresh.setLoadColors()
        adapter = GiveawayAdapter(requireActivity())
        rvGiveaways.initializeGridVertical(adapter, 2)
    }


}