package com.giveaway.pak.view.giveaway

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.giveaway.pak.R
import com.giveaway.pak.model.GiveawayModel
import com.giveaway.pak.util.GIVEAWAY_MODEL
import com.giveaway.pak.util.loadImageFromUrl
import com.giveaway.pak.view.NavigationManager
import kotlinx.android.synthetic.main.item_giveaways.view.*

class GiveawayAdapter (private val context: Context): RecyclerView.Adapter<GiveawayAdapter.GiveawayViewHolder>() {


    private var giveawaysList : ArrayList<GiveawayModel> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiveawayViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_giveaways,parent, false)
        return GiveawayViewHolder((view))
    }

    override fun getItemCount(): Int {
       return giveawaysList.size
    }

    override fun onBindViewHolder(holder: GiveawayViewHolder, position: Int) {
        holder.setView(giveawaysList[position])
    }

    inner class GiveawayViewHolder (itemView: View): RecyclerView.ViewHolder(itemView) {

        private val tvTitle : AppCompatTextView = itemView.tvTitle
        private val tvLocation : AppCompatTextView = itemView.tvLocation
        private val ivImage : AppCompatImageView = itemView.ivImage
        init {
            itemView.setOnClickListener {
                gotoDetailsScreen()
            }
        }

        private fun gotoDetailsScreen() {
            val bundle  = Bundle()
            bundle.putSerializable(GIVEAWAY_MODEL, giveawaysList[adapterPosition])
            NavigationManager.openClass(context,GiveawayDetailsActivity::class.java,bundle)
        }

        fun setView(giveawayModel: GiveawayModel) {
            tvTitle.text = giveawayModel.title
            tvLocation.text = giveawayModel.city
            giveawayModel.imageURLs?.get(0)?.let { ivImage.loadImageFromUrl(context, it) }
        }

    }

    fun addAll(giveawayList: List<GiveawayModel>){
        giveawaysList.addAll(giveawayList)
        notifyDataSetChanged()
    }

    fun deleteAll(){
        giveawaysList.clear()
        notifyDataSetChanged()
    }

}