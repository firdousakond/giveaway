package com.giveaway.pak.view.common

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.giveaway.pak.R
import com.giveaway.pak.model.GiveawayModel
import com.giveaway.pak.util.GIVEAWAY_MODEL
import com.giveaway.pak.util.Logger
import com.giveaway.pak.util.hide
import com.giveaway.pak.view.giveaway.SlidingImageAdapter
import kotlinx.android.synthetic.main.activity_full_image.*
import kotlinx.android.synthetic.main.activity_full_image.indicator
import kotlinx.android.synthetic.main.activity_full_image.vpImages
import java.util.*

class FullImageActivity : AppCompatActivity(), SlidingImageAdapter.ImageClickListener {

    private lateinit var giveawayModel: GiveawayModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_image)
        getBundle()
        iVBack.setOnClickListener { finish() }
    }

    private fun getBundle(){
        val bundle = intent.extras
        if(bundle != null){
            giveawayModel = bundle.getSerializable(GIVEAWAY_MODEL) as GiveawayModel
            setImageSlider()

        }
    }

    private fun setImageSlider() {
        if (giveawayModel.imageURLs?.size!! < 2) {
            indicator.hide()
        }
        val imageAdapter =
            SlidingImageAdapter(this, giveawayModel.imageURLs, this)
        vpImages.adapter = imageAdapter
        indicator.setupWithViewPager(vpImages)
        val timer = Timer()
        timer.scheduleAtFixedRate(SliderTimer(), 3000, 3000)

    }

    private inner class SliderTimer : TimerTask() {

        override fun run() {
            this@FullImageActivity.runOnUiThread {
                if (vpImages.currentItem < giveawayModel.imageURLs?.size!!.minus(1)) {
                    vpImages.currentItem = vpImages.currentItem + 1
                } else {
                    vpImages.currentItem = 0
                }
            }
        }
    }

    override fun onImageClick() {
        Logger.debug("Image Clicked")
    }
}