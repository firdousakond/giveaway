package com.giveaway.pak.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import com.giveaway.pak.R
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern


object CommonUtil {

    fun displaySnackbar(
        activity: Activity?,
        message: String,
        messageType: MessageType
    ) {

        if (activity != null) {

            var view: View? = activity.window.decorView.rootView ?: return

            try {
                view =
                    (activity.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(
                        0
                    )
            } catch (e: Exception) {
                Log.d("Error", e.toString())
            }

            val snackbar = Snackbar.make(view!!, message, Snackbar.LENGTH_SHORT)

            val tvSnackBar =
                snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)

            tvSnackBar.textAlignment = View.TEXT_ALIGNMENT_CENTER

            when (messageType) {

                MessageType.ERROR -> tvSnackBar.setBackgroundColor(
                    activity.resources.getColor(
                        R.color.red_color
                    )
                )
                MessageType.SUCCESS -> tvSnackBar.setBackgroundColor(
                    activity.resources.getColor(
                        R.color.green_color
                    )
                )
                else -> tvSnackBar.setBackgroundColor(activity.resources.getColor(R.color.grey_color))
            }

            snackbar.view.setPadding(0, 0, 0, 0)

            snackbar.show()

        }
    }


    fun isNetworkAvailable(context: Context): Boolean {
        val connectivity =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.allNetworkInfo
            if (info != null) {
                for (anInfo in info) {
                    if (anInfo.state == NetworkInfo.State.CONNECTED) {
                        return true
                    }
                }
            }
        }
        return false
    }

    fun isValidEmail(inputEmail: String): Boolean {
        val regExpn = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")
        val patternObj = Pattern.compile(regExpn)

        val matcherObj = patternObj.matcher(inputEmail)
        return matcherObj.matches()
    }




}