package com.giveaway.pak.view.entry

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.giveaway.pak.model.GiveawayModel
import com.giveaway.pak.model.UserModel
import com.giveaway.pak.util.LiveDataResult
import com.giveaway.pak.util.Logger
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.base.BaseViewModel
import com.google.firebase.firestore.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class EntryViewModel @Inject constructor(
    private var firestore: FirebaseFirestore
) :
    BaseViewModel<BaseHandler>() {


    var entryLiveData = MutableLiveData<LiveDataResult<Boolean>>()


    fun addEntries(giveawayModel: GiveawayModel, userModel: UserModel?) {
        val entries = hashMapOf("imageUrl" to userModel?.imageURL,
        "uid" to userModel?.id, "name" to userModel?.name, "email" to userModel?.email)

        viewModelScope.launch(Dispatchers.IO) {
            try {
                entryLiveData.postValue(LiveDataResult.loading())
                firestore.collection("giveaway").document(giveawayModel.id!!)
                    .update("entries",FieldValue.arrayUnion(entries))
                    .addOnSuccessListener {
                        entryLiveData.postValue(LiveDataResult.success(true))
                    }
                    .addOnFailureListener {
                        entryLiveData.postValue(LiveDataResult.error(it))
                    }
            }
            catch (ex: Exception){
                Logger.error(ex.message.toString())
            }
        }
    }


}