package com.giveaway.pak.util

import android.annotation.SuppressLint
import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore

import java.io.BufferedOutputStream
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.FileWriter
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.io.PrintWriter
import java.util.ArrayList

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

object FileUtils {

    /**
     * Write Data to a file on the specified location
     *
     * @param message
     * @param path
     * @param fileName
     * @return
     */
    fun writeToFile(message: String, path: String, fileName: String) {

        try {

            val dir = getFile(path)
            dir.mkdirs()
            val file = getFile(dir, fileName)
            PrintWriter(
                BufferedWriter(
                    FileWriter(file, true), 8 * 1024
                )
            ).use { writer ->
                writer.print(message)
                file.absolutePath
                writer.flush()
            }
        } catch (e: Exception) {
            Logger.error(e.message!!)
        }

    }

    /**
     * Write Data to a file on the specified location
     *
     * @param data
     * @param path
     * @param fileName
     * @return
     */
    fun writeByteArrayToFile(data: ByteArray, path: String, fileName: String) {
        try {
            val dir = getFile(path)
            dir.mkdirs()

            val file = getFile(dir, fileName)
            FileOutputStream(file).use { fos ->
                BufferedOutputStream(fos).use { bos ->
                    bos.write(data)
                    file.absolutePath

                    bos.flush()
                    fos.flush()
                }
            }
        } catch (e: Exception) {
            Logger.error(e.message!!)
        }

    }


    @Throws(IOException::class)
    private fun convertStreamToString(`is`: InputStream): String {
        val reader = BufferedReader(InputStreamReader(`is`))
        val sb = StringBuilder()
        val line = reader.readLine()
        while (line != null) {
            sb.append(line).append("\n")
        }
        reader.close()
        return sb.toString()
    }

    /**
     * Read Data from the File
     *
     * @param filePath
     * @return
     * @throws Exception
     */
    @Throws(IOException::class)
    fun readStringFromFile(filePath: String): String {
        val fl = getFile(filePath)
        val fin = FileInputStream(fl)
        val ret = convertStreamToString(fin)
        //Make sure you close all streams.
        fin.close()
        return ret
    }

    /**
     * Read Data from the File
     *
     * @param filePath
     * @return
     * @throws Exception
     */
    fun readByteArrayFromFile(filePath: String): ByteArray {

        val file = getFile(filePath)

        try {

            FileInputStream(file).use { inputStream ->

                // Get the size of the file
                val length = file.length()

                // You cannot create an array using a long type.
                // It needs to be an int type.
                // Before converting to an int type, check
                // to ensure that file is not larger than Integer.MAX_VALUE.
                if (length > Integer.MAX_VALUE) {
                    throw IOException(
                        "Could not completely read FILE " + file.name
                                + " as it is too long (" + length + " bytes, max supported " + Integer.MAX_VALUE + ")"
                    )
                }

                // Create the byte array to hold the data
                val bytes = ByteArray(length.toInt())

                // Read in the bytes
                var offset = 0
                var numRead =  inputStream.read(bytes, offset, bytes.size - offset)
                while (offset < bytes.size && numRead>= 0) {
                    offset += numRead
                }

                // Ensure all the bytes have been read in
                if (offset < bytes.size) {
                    throw IOException("Could not completely read FILE " + file.name)
                }

                return bytes
            }
        } catch (e: Exception) {
            Logger.error(e.message!!)
        }

        return byteArrayOf()
    }

    /**
     * Check if the file exist at the given file path
     *
     * @param path
     * @return
     */
    fun ifFileExists(path: String): Boolean {
        val file = getFile(path)
        return file.exists()
    }

    /**
     * Provides internal file path for app
     *
     * @param context
     * @return
     */
    fun getInternalFilePath(context: Context?): String? {
        if (context != null) {
            val externalFileDirectory = context.filesDir
            return if (externalFileDirectory != null) {
                context.filesDir.absolutePath
            } else {
                null
            }
        } else {
            return null
        }
    }

    /**
     * No sonar added for security hotspot
     *
     * @param path
     * @return
     */
    fun getFile(path: String): File {
        return File(path)//NOSONAR
    }

    /**
     * No sonar added for security hotspot
     *
     * @param path
     * @return
     */
    internal fun getFile(dir: File, path: String): File {
        return File(dir, path)//NOSONAR
    }


    /**
     * No sonar added for security hotspot
     *
     * @param path
     * @return
     */
    fun getFileOutputStream(path: String): FileOutputStream? {
        try {
            return FileOutputStream(path)//NOSONAR
        } catch (e: Exception) {
            return null
        }

    }

    fun getImageListMultipart(imagesList: List<String>): List<MultipartBody.Part> {

        val parts = ArrayList<MultipartBody.Part>()

        for (i in imagesList.indices) {
            parts.add(prepareImageListFilePart("images[$i]", imagesList[i]))
        }
        return parts
    }

    private fun prepareImageListFilePart(partName: String, filePath: String): MultipartBody.Part {

        val file = File(filePath)

        val requestBody = RequestBody.create(MediaType.parse("image/*"), file)

        return MultipartBody.Part.createFormData(partName, file.name + ".png", requestBody)
    }


    fun getImageMultipart(path: String): MultipartBody.Part {

        val part: MultipartBody.Part

        part = prepareImageFilePart(path)
        return part
    }

    private fun prepareImageFilePart(filePath: String): MultipartBody.Part {

        val file = File(filePath)

        val requestBody = RequestBody.create(MediaType.parse("image/*"), file)

        return MultipartBody.Part.createFormData("fileName", file.name + ".png", requestBody)
    }

    fun getPdfMultipart(path: String): MultipartBody.Part {

        val part: MultipartBody.Part

        part = preparePdfFilePart(path)
        return part
    }

    private fun preparePdfFilePart(filePath: String): MultipartBody.Part {

        val file = File(filePath)

        val requestBody = RequestBody.create(MediaType.parse("application/pdf"), file)

        return MultipartBody.Part.createFormData("fileName", file.name, requestBody)
    }


    fun getRealPath(context: Context, fileUri: Uri): String? {
        val realPath: String?
        // SDK < API11
        realPath = getRealPathFromURI_API19(context, fileUri)
        return realPath
    }


    @SuppressLint("NewApi")
    private fun getRealPathFromURI_API19(context: Context, uri: Uri): String? {


        // DocumentProvider
        if (DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                // This is for checking Main Memory
                return if ("primary".equals(type, ignoreCase = true)) {
                    if (split.size > 1) {
                        Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                    } else {
                        Environment.getExternalStorageDirectory().toString() + "/"
                    }
                    // This is for checking SD Card
                } else {
                    "storage" + "/" + docId.replace(":", "/")
                }

            } else if (isDownloadsDocument(uri)) {
                val fileName = getFilePath(context, uri)
                if (fileName != null) {
                    return Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName
                }

                var id = DocumentsContract.getDocumentId(uri)
                if (id.startsWith("raw:")) {
                    id = id.replaceFirst("raw:".toRegex(), "")
                    val file = File(id)
                    if (file.exists())
                        return id
                }

                val contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"),
                    java.lang.Long.valueOf(id)
                )
                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(context, contentUri, selection, selectionArgs)
            }// MediaProvider
            // DownloadsProvider
        } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                context,
                uri,
                null,
                null
            )

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)

        return null
    }

    private fun getDataColumn(
        context: Context, uri: Uri?, selection: String?,
        selectionArgs: Array<String>?
    ): String? {

        val column = "_data"
        val projection = arrayOf(column)
        context.contentResolver.query(
            uri!!,
            projection,
            selection,
            selectionArgs,
            null
        )!!.use { cursor ->
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        }
        return null
    }


    private fun getFilePath(context: Context, uri: Uri): String? {

        val projection = arrayOf(MediaStore.MediaColumns.DISPLAY_NAME)
        context.contentResolver.query(uri, projection, null, null, null)!!.use { cursor ->
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME)
                return cursor.getString(index)
            }
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }


}// empty constructor
