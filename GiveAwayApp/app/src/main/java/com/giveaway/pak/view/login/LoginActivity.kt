package com.giveaway.pak.view.login

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import com.facebook.*
import com.facebook.login.LoginResult
import com.giveaway.pak.BR
import com.giveaway.pak.R
import com.giveaway.pak.databinding.ActivityLoginBinding
import com.giveaway.pak.model.UserModel
import com.giveaway.pak.model.WalkThroughModel
import com.giveaway.pak.util.CommonUtil.displaySnackbar
import com.giveaway.pak.util.LiveDataResult
import com.giveaway.pak.util.Logger
import com.giveaway.pak.util.MessageType
import com.giveaway.pak.util.SharedPrefUtil
import com.giveaway.pak.view.NavigationManager
import com.giveaway.pak.view.base.BaseBindingActivity
import com.giveaway.pak.view.base.BaseHandler
import com.giveaway.pak.view.home.HomeActivity
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*
import javax.inject.Inject


class LoginActivity : BaseBindingActivity<ActivityLoginBinding, LoginViewModel, BaseHandler>() {

    @Inject
    lateinit var loginViewModel: LoginViewModel

    override fun getViewModel(): LoginViewModel? = loginViewModel

    override fun getBindingVariable(): Int = BR.viewmodel

    override fun getLayoutId(): Int = R.layout.activity_login

    private var callbackManager: CallbackManager? = null
    private var accessToken: AccessToken? = null
    private var facebookToken = ""
    private var facebookId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding?.handler = this
        initObservers()
        setListener()
        setupWalkThrough()
    }

    private fun setupWalkThrough() {
        val pagerAdapter = ScreenSlidePagerAdapter(supportFragmentManager)
        vpWalkThrough.adapter = pagerAdapter
        tlIndicator.setupWithViewPager(vpWalkThrough)
    }

    private fun setListener() {
        llFacebook.setOnClickListener {
            callFacebookLogin()
        }
    }

    private fun initObservers() {
        loginViewModel.userLiveData.observe(this, userObserver)
        loginViewModel.userDetailsLiveData.observe(this, userDetailsObserver)
    }

    private val userObserver = Observer<LiveDataResult<QuerySnapshot>> { result ->
        when (result.status) {
            LiveDataResult.Status.LOADING -> {
                onShowLoading()
            }
            LiveDataResult.Status.SUCCESS -> {
                onDismissLoading()
                if (result.data != null) {
                    val documentSnapshot = result.data.documents[0]
                    val userModel = documentSnapshot.toObject(UserModel::class.java)
                    if (userModel != null) {
                        saveUser(userModel)
                    }
                }
            }
            LiveDataResult.Status.ERROR -> {
                onDismissLoading()
                displaySnackbar(
                    this,
                    getString(R.string.common_error_msg),
                    MessageType.ERROR
                )
            }

        }

    }

    private val userDetailsObserver = Observer<LiveDataResult<QuerySnapshot>>{
        result->
        when(result.status){

            LiveDataResult.Status.LOADING->{
                onShowLoading("")
            }
            LiveDataResult.Status.SUCCESS-> {
                onDismissLoading()
                if(result.data != null && !result.data.documents.isNullOrEmpty()){
                    val userModel = result.data.documents[0].toObject(UserModel::class.java)
                    if (userModel != null) {
                       saveUser(userModel)
                    }else{
                        registerUser(accessToken)
                    }
                }else{
                    registerUser(accessToken)
                }
            }

            LiveDataResult.Status.ERROR-> {
                onDismissLoading()
                registerUser(accessToken)
            }

        }

    }

    private fun saveUser(userModel: UserModel){
        SharedPrefUtil(this).setUserId(userModel.id!!)
        SharedPrefUtil(this).setFacebookId(userModel.facebookId!!)
        NavigationManager.openClass(this, HomeActivity::class.java,null)
        finish()
    }
    private fun registerUser(accessToken: AccessToken?) {
        val userModel = UserModel()
        val userId = UUID.randomUUID().toString().toUpperCase(Locale.ENGLISH)
        userModel.id = userId
        userModel.facebookId = facebookId

        val request = GraphRequest.newMeRequest(
            accessToken
        )
        { json, _ ->

            if (json.has("name")) {
                userModel.name = json.getString("name")
            }
            if (json.has("email")) {
                userModel.email = json.getString("email")
            }

            if (json.has("picture")) {
                val profilePicUrl =
                    json.getJSONObject("picture").getJSONObject("data").getString("url")
                userModel.imageURL = profilePicUrl
            }

            loginViewModel.registerUser(userModel)

        }
        val parameters = Bundle()
        parameters.putString(
            "fields",
            "id,name,first_name,last_name,email,gender,birthday,picture.type(large)"
        )

        request.parameters = parameters
        request.executeAsync()
    }

    private fun callFacebookLogin() {
        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedIn = accessToken != null && !accessToken.isExpired
        if (isLoggedIn) {
            facebookToken = accessToken.token
            facebookId = accessToken.userId
            registerUser(accessToken)
        } else {
            networkCallGetFaceBookSession()
            fbLogin.performClick()
        }
    }

    private fun networkCallGetFaceBookSession() {
        fbLogin.setReadPermissions(listOf("email", "public_profile"))
        callbackManager = CallbackManager.Factory.create()
        fbLogin.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult?) {
                if (loginResult != null) {
                    this@LoginActivity.accessToken = loginResult.accessToken
                    facebookId = loginResult.accessToken.userId
                    facebookToken = loginResult.accessToken.token
                    loginViewModel.fetchUser(facebookId)
                }

            }

            override fun onCancel() {
                displaySnackbar(this@LoginActivity, "Login Cancelled", MessageType.ERROR)
            }

            override fun onError(exception: FacebookException) {
                // App code
                Logger.error(exception.message.toString())

            }
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        onDismissLoading()
        if (callbackManager != null) {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }

    }

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(
        fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
        private val walkThroughList = WalkThroughModel.getDefaultWalkThrough(this@LoginActivity)
        override fun getCount(): Int = walkThroughList.size

        override fun getItem(position: Int): Fragment = WalkThroughFragment().newInstance(
            walkThroughList,
            position
        )

    }
}